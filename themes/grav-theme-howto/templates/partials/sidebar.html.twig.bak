
<div id="navigation">
  {% if theme_var('github.link') %}
    {% include 'partials/github-link.html.twig' %}
  {% endif %}
  {% if not progress.isFirst(page.path) %}
    <!--a class="nav-prev tooltip tooltip-bottom" data-tooltip="Previous Page [&larr;]" href="{{ progress.nextSibling(page.path).url }}">{{ progress.nextSibling(page.path).title }} <i class="fa fa-angle-left"></i></a-->
  {% else %}
    <!--span class="disabled"><i class="fa fa-angle-left"></i></span-->
  {% endif %}
  {% if not progress.isLast(page.path) %}
    <a class="nav-next tooltip tooltip-bottom push-right" data-tooltip="Next Page [&rarr;]" href="{{ progress.prevSibling(page.path).url }}"><i class="fa fa-angle-right"></i> NEXT PAGE: {{ progress.prevSibling(page.path).title }}</a>
  {% else %}
    <span class="disabled"><i class="fa fa-angle-right"></i></span>
  {% endif %}
</div>
<div class="progress"></div>

{% if config.get('plugins.page-toc.active') or attribute(page.header, 'page-toc').active %}
  <div class="page-toc">
    {% set table_of_contents = toc(page.content) %}
    {% if table_of_contents is not empty %}
      <span class="toc-toggle"><i class="fa fa-angle-up"></i></span>
      <h5>Table of Content</h5>
      {{ table_of_contents|raw }}
    {% endif %}
  </div>
{% endif %}

{% if config.plugins.relatedpages.enabled and related_pages|length > 0 %}
    <h4>Related Posts</h4>
    {% include 'partials/relatedpages.html.twig' %}
{% endif %}

{% set homeroute = '/home' %}
{% if grav.theme.config.params.articleroot is defined %}
    {% set homeroute = grav.theme.config.params.articleroot %}
{% endif %}
{% if grav.theme.config.params.articles.root is defined %}
    {% set homeroute = grav.theme.config.params.articles.root %}
{% endif %}

{% set options = { items: {'@page.descendants': homeroute}, 'order': {'by': 'date', 'dir': 'desc'}} %}
{% set my_collection = page.collection(options) %}

{% set catlist = taxonomy.taxonomy["category"]|keys %}
{% set blist = [] %}
{% if grav.theme.config.params.articles.blacklist is defined %}
    {% set blist = grav.theme.config.params.articles.blacklist %}
    {% set tmplst = [] %}
    {% for cat in catlist %}
        {% if cat not in blist %}
            {% set tmplst = tmplst|merge([cat]) %}
        {% endif %}
    {% endfor %}
    {% set catlist = tmplst %}
{% endif %}

{% if grav.theme.config.params.sidebar.show.categories %}
<div>
    <h4><span>{{ 'CATEGORIES'|t }}</span></h4>
    <ul style="padding-left: 1rem">
    {% for cat in catlist|sort %}
        {# Check to see if a dedicated category page exists #}
        {% set slug = cat|hyphenize|url_encode %}
        {% set p = page.find('/categories/'~slug) %}
        {% if p == null %}
                <li><a href="{{ base_url }}/taxonomy?name=category&amp;val={{ cat|url_encode }}">{{ cat }}</a></li>
        {% else %}
                <li><a href="{{ base_url }}/categories/{{slug}}">{{ cat }}</a></li>
        {% endif %}
    {% endfor %}
    </ul>
</div>
{% endif %}

{% set maxcount = 5 %}
{% if grav.theme.config.params.sidebar.maxentries is defined %}
    {% set maxcount = grav.theme.config.params.sidebar.maxentries %}
{% endif %}

{% if grav.theme.config.params.sidebar.show.popular %}
<div class="topiclist">
	{% set counts = viewcounts|sort|reverse %}
	{% set popular = [] %}
	{% for route,views in counts %}
		{% if route starts with homeroute %}
            {% set thispage = page.find(route) %}
            {% if thispage is not null %}
			    {% set popular = popular|merge([thispage]) %}
            {% endif %}
		{% endif %}
	{% endfor %}
    {% set tmplst = [] %}
    {% for page in popular %}
        {% set blisted = false %}
        {% for bcat in blist %}
            {% if bcat in page.taxonomy["category"] %}
                {% set blisted = true %}
            {% endif %}
        {% endfor %}
        {% if not blisted %}
            {% set tmplst = tmplst|merge([page]) %}
        {% endif %}
    {% endfor %}
    {% set popular = tmplst %}
	<h4><span>{{ 'POPULAR_ARTICLES'|t }}</span></h4>
    {% include 'partials/topiclist.html.twig' with {'articles': popular, 'maxcount': maxcount} %}
</div>
{% endif %}

{% if grav.theme.config.params.sidebar.show.latest %}
<div class="topiclist">
	<h4><span>{{ 'LATEST_ARTICLES'|t }}</span></h4>
    {% set tmplst = [] %}
    {% for page in my_collection %}
        {% set blisted = false %}
        {% for bcat in blist %}
          {% if bcat in page.taxonomy["category"] or (page.header.published is defined and not page.header.published) %}
          {# Setting this to blacklisted when the category is in the blacklist OR if the page isn't yet published #}
            {% set blisted = true %}
          {% endif %}
        {% endfor %}
        {% if not blisted %}
            {% set tmplst = tmplst|merge([page]) %}
        {% endif %}
    {% endfor %}
    {% set articles = tmplst|slice(0,maxcount) %}
    {% include 'partials/topiclist.html.twig' with {'articles': articles, 'maxcount': maxcount} %}
</div>
{% endif %}
