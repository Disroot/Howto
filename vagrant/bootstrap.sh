#!/usr/bin/env bash

# Custom configuration
WEB_NAME="howto.disroot.lan"
WEB_ROOT="/var/www/"
WWW_USER="www-data"
SITE_PATH="${WEB_ROOT}${WEB_NAME}/site"
YAML_SYSTEM="${SITE_PATH}/user/config/system.yaml"
YAML_SITE="${SITE_PATH}/user/config/site.yaml"
GRAV_VERSION="1.7.45"
PHP_VERSION="8.2"
DISTRO="bookworm"


# Provisioning actions
apt install dialog

# Avoid Postfix installation interactive screens by preconfiguring this information
sudo debconf-set-selections <<< "postfix postfix/main_mailer_type select No configuration"
sudo debconf-set-selections <<< "postfix postfix/mailname string ${WEB_NAME}"

# Add sury repository to sources.list for php
echo "set grub-pc/install_devices /dev/sda" | debconf-communicate # Fix grub error
sudo apt-get -y update
sudo apt-get -y dist-upgrade
sudo apt-get -y install ca-certificates apt-transport-https

## Check if the source list file exists
if grep -q "^deb .*sury" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
    # If the file exists, update it
    sudo sed -i "s|deb https://packages.sury.org/php/ .* main|deb https://packages.sury.org/php/ ${DISTRO} main|" /etc/apt/sources.list.d/php.list
else
    # If the file does not exist, create it
    sudo sh -c "echo 'deb https://packages.sury.org/php/ ${DISTRO} main' > /etc/apt/sources.list.d/php.list"
fi

# Sury Key for php
wget -q https://packages.sury.org/php/apt.gpg -O- | sudo tee /etc/apt/trusted.gpg.d/sury-php.gpg > /dev/null
sudo apt-get -y update
sudo apt-get -y dist-upgrade

echo "Installing nginx..."
sudo apt-get install -y nginx-full

echo "Installing composer..."
sudo apt-get install -y composer

echo "Installing php"${PHP_VERSION}"..."
sudo apt-get install -y php"${PHP_VERSION}" php"${PHP_VERSION}"-zip php"${PHP_VERSION}"-cli php"${PHP_VERSION}"-curl php"${PHP_VERSION}"-gd php"${PHP_VERSION}"-mbstring php"${PHP_VERSION}"-xml php"${PHP_VERSION}"-fpm

# Create the Nginx config files and restart webserver
echo "Installing Nginx config files..."
sudo rsync -cr /vagrant/provision/etc/nginx/sites-available /etc/nginx/
if [ ! -f /etc/nginx/sites-enabled/"${WEB_NAME}".conf ]; then
    sudo ln -s /etc/nginx/sites-available/"${WEB_NAME}".conf /etc/nginx/sites-enabled/"${WEB_NAME}".conf
fi
if [ -f /etc/nginx/sites-enabled/default ]; then
    sudo rm /etc/nginx/sites-enabled/default
fi
sudo service nginx restart
 
# Install GRAV in webroot
echo "Installing GRAV..."
sudo chown -R ${WWW_USER}:${WWW_USER} "${WEB_ROOT}"

if [ ! -d "${SITE_PATH}" ]; then
   sudo -u "${WWW_USER}" mkdir "${SITE_PATH}"
fi

# Install needed packages
echo "Installing needed packages..."
sudo apt-get install -y unzip

## Specific version
FILE="${WEB_ROOT}"grav-v"${GRAV_VERSION}".zip
if [ ! -f "$FILE" ]; then
  echo "Downloading grav-v"${GRAV_VERSION}".zip"
  sudo -u "${WWW_USER}" wget https://github.com/getgrav/grav/releases/download/"${GRAV_VERSION}"/grav-v"${GRAV_VERSION}".zip -P "${WEB_ROOT}"
  sudo -u "${WWW_USER}" unzip -o "${WEB_ROOT}"grav-v"${GRAV_VERSION}".zip -d "${WEB_ROOT}"
  sudo -u "${WWW_USER}" cp -r "${WEB_ROOT}"grav/* "${SITE_PATH}"
else
  echo "grav-v"${GRAV_VERSION}".zip already exists"
  sudo -u "${WWW_USER}" unzip -o "${WEB_ROOT}"grav-v"${GRAV_VERSION}".zip -d "${WEB_ROOT}"
  sudo -u "${WWW_USER}" cp -r "${WEB_ROOT}"grav/* "${SITE_PATH}"
fi


echo "Enter grav folder"
cd "${SITE_PATH}"

# Install modules
# https://github.com/sommerregen/grav-plugin-themer
yes | sudo -u "${WWW_USER}" php"${PHP_VERSION}" bin/gpm install themer
yes | sudo -u "${WWW_USER}" php"${PHP_VERSION}" bin/gpm install simplesearch
yes | sudo -u "${WWW_USER}" php"${PHP_VERSION}" bin/gpm install relatedpages
yes | sudo -u "${WWW_USER}" php"${PHP_VERSION}" bin/gpm install breadcrumbs
yes | sudo -u "${WWW_USER}" php"${PHP_VERSION}" bin/gpm install page-toc
yes | sudo -u "${WWW_USER}" php"${PHP_VERSION}" bin/gpm install tagcloud

# Add Grav config (the second extra false should be set to true, but it creates an error in Grav ("Trying to access array offset on value of type null")
# Create the Nginx config files and restart webserver
echo "Installing Grav config file config file..."
sudo rsync -cr "/vagrant/provision${YAML_SYSTEM}" "${YAML_SYSTEM}"
sudo rsync -cr "/vagrant/provision${YAML_SITE}" "${YAML_SITE}"

# Add website domain to local /etc/hosts file
sudo sed -i "s/127.0.0.1\tlocalhost/127.0.0.1\tlocalhost $WEB_NAME/" /etc/hosts

# Add website pages in GRAV
echo "Installing Disroot Howto pages in GRAV..."
sudo mount -o bind "${WEB_ROOT}""${WEB_NAME}"/disroot.lan-pages/pages "${SITE_PATH}"/user/pages
chown "${WWW_USER}":"${WWW_USER}" -R "${SITE_PATH}"/user/pages

# Add Disroot Howto theme in GRAV
echo "Installing Disroot theme in GRAV..."
if [ ! -d "${SITE_PATH}"/user/themes/grav-theme-howto ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}""${WEB_NAME}"/disroot.lan-theme "${SITE_PATH}"/user/themes/grav-theme-howto
fi

# Add language-selector in GRAV
echo "Installing language-selector in GRAV..."
if [ ! -d "${SITE_PATH}"/user/plugins/language-selector ]; then
    sudo -u "${WWW_USER}" ln -s "${WEB_ROOT}""${WEB_NAME}"/grav-plugin-language-selector "${SITE_PATH}"/user/plugins/language-selector
fi

# Remove the default grav files and folder from new installation
echo "Remove the default grav files and folder from new installation..."
if [ -d "${SITE_PATH}"/user/pages/02.typography ]; then
    rm -r "${SITE_PATH}"/user/pages/02.typography
fi
if [ -f "${SITE_PATH}"/user/pages/01.home/default.md ]; then
    rm  "${SITE_PATH}"/user/pages/01.home/default.md
fi

exit 0
