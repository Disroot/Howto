<p align="center">

![](h2.png)

</p>

<h1 align="center">Gvidiloj kaj Tradukoj de Disroot</h1>

<h2 align="center">· Aliĝu kaj kunlaboru ·</h2>

Tiu ĉi deponejo enhavas la dosieroj de la **Howto-a Projekto de Disroot** kaj la informo por krei, modifi kaj traduki tiuj dosieroj.

La ĉefa objektivo de la **Howto-a Projekto de Disroot** estas la kreo, teno kaj alireblo de gvidiloj por geuzantoj, lerniloj kaj agordoj por la diversaj serviloj kaj programaro, kiun la platformo **Disroot** donas. Kaj ankaŭ doni la iloj, instrukcioj kaj procezoj por iu ajn kiu volas kontribui kreante, modifante kaj tradukante tiuj ĉi gvidiloj.

<br>

# Antaŭkondiĉo. 

- a. Necesa programaro.
- b. Konto en **Gitea** apero
- c. Peti aliro al niaj deponejoj

Por povi akiri kopio de la projekto en loka maŝino kaj komenci je labori, vi devas sekvi tiuj ĉi instrukcioj.

## a. Necesa programaro
Unue, vi bezonas havi instalita la sekva programaro.

- ## git<br>
Kontrolversia sistemo por la sekvo de la ŝanĝoj kiu estas efektivigitaj en la gvidiloj kaj tradukoj dum ili estas skribitaj.<br>
[**Kio estas git?**](https://eo.wikipedia.org/wiki/Git) <br> · [**Elŝuto kaj instalaj instrukcioj**](https://git-scm.com/downloads)

## Tekstredaktilo kun subteno por dosieroj kun markdown-a formato

Sugestaj programoj:_:<br>
· **VSCodium**, komunumadministra versio kun libera permesilo de la redaktilo de Microsoft. [[**Elŝuto kaj instalaj instrukcioj**](https://vscodium.com/)]<br>
· **Neovim**, tekstredaktilo hiperaldonema bazita je Vim. [[**Elŝuto kaj instalaj instrukcioj**](https://github.com/neovim/neovim/wiki/Installing-Neovim)]<br>
· **Kate Editor**, redaktilo plena de  karakterizaĵo. [[**Elŝuto kaj instalaj instrukcioj***](https://kate-editor.org/es/get-it/)]

<br>

## b. Gitea konto
Por povi sendi la ŝanĝoj kiu faras al la gvidiloj, jam esti tradukante, eldonante aŭ skribante, vi bezonas havi konton en **Gitea** apero.

Por registrigi konton en la apero kie estas niaj deponejoj, iru [**ĉi tie**](https://git.disroot.org/user/sign_up).

<br>

## c. Peti aliro al nia deponejo
Por povi registrigi ŝanĝoj en la git-a deponejoj de **Disroot**, vi bezonos peti aliron. Oni faras tion ĉi ekde nia git-a retejo de nia projekto.

<br>

## Agordi nian uzantnomo kaj retpoŝtadreso
Tio ĉi necesas por povi sendi vian laboron de via loka maŝino al interreta deponejo. Por agordi la uzantnomo kaj la retpoŝtadreso de git.

-- malfermu terminalo kaj komenciĝu git<br>
`git init`<br>

- Tajpu, kaj plenigi kun via informo, la sekvaj komandoj:<br>
`git config --global user.email "uzanto@retpoŝtadreso"`<br>
`git config --global user.name "Uzantnomo"`

<br>

# 2. Kreante / Tradukante gvidiloj
La procezo de kreado aŭ traduko de gvidiloj ne estas malfacila kaj bezonas nur iom da praktiko por lerni.

La procezo estas tiom simpla:

1. Akiri kopio de la dosieroj je kiu vi laboros;
2. Laboru loke en via komputilo je la dosieroj,
3. Kaj kiam vi finos, sendu ilin.

Unue kiu oni devas fari estas kloni la deponejo, tio ĉi signifas fari loka kopio de la interreta dosieroj. La tuta laboro fariĝos je tiu ĉi kopio.

<br>

## a. Kloni la deponejo
Git-a deponejo estas projekta dosierujo kiun enhavas la dosieroj kreitaj aŭ je esti tradukotaj kaj la detala historia registro kun la ŝanĝoj.

Por kloni la deponejo, malfermu terminalo kaj en la dosierujo en kiu vi ŝatus kloni ĝin. Unufoje tie vi uzus la komando `git clone`, kaj instrukcio por kloni deponejo tajpante sian adreso. En nia kazo, ĝi estus:

`git clone https://git.disroot/Disroot/Howto`

Unufoje kiu la deponejo estis kopiita al nia fiksita diskilo, vi vidos dosierujo `howto` kiu enhavas ĉiuj la dosieroj de la projekto. Poste vi povas movi al ie ajn en via komputilo.

<br>

## Instrukcioj por krei / traduki
Unufoje vi instalos la deponejo en via maŝino, vi povas komenci je krei, eldoni kaj/aŭ traduki sekvante la paŝoj priskribitaj en tiuj ĉi gvidiloj

/Ĝis la gvidiloj ne estos tradukitaj mi lasos vin la anglaj version. Pardonu/

- [Kiel kontribui al Howto-a projekto de **Disroot**](https://howto.disroot.org/en/contribute)<br>

- [Procezo por Gvidiloj kaj Tradukoj](https://howto.disroot.org/en/contribute/procedure)

<br>

# Permesilo
La dokumentaro de **Howto-a Disroot** estas sub rajtilo [Atribuite-Samkondiĉe 4.0 Tutmonda de Creative Commons](https://creativecommons.org/licenses/by-sa/4.0/deed.eo)
