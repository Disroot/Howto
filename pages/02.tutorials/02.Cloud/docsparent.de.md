---
title: Cloud
subtitle: "Basics, Einstellungen, Synchronisation, Clients"
icon: fa-cloud
updated:
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - cloud
        - nextcloud
page-toc:
    active: false
---

![](nc_logo.png)

**Nextcloud** ist eine freie, quelloffene Software, die es Dir ermöglicht, Dateien auf einem Server (der auch Dein eigener sein kann) hochzuladen und zu speichern, sie zwischen verschiedenen Geräten zu synchronisieren und von überall via Internet sicher darauf zuzugreifen. Einige der Features, die es bietet, sind: Kalender-, Kontakte- und Lesezeichen-Synchronisation, Anrufe / Videokonferenzen, ein grundlegendes Projektboard, ein News-Feeder und mehr<br>
Es ist **Disroot**'s Kernservice und das Haupt-Userinterface, das wir in die meisten Apps, die wir anbieten, zu integrieren versuchen.

In den folgenden Kapiteln dieses **Cloud**-Howto werden wir mehr über das Interface, die Grundlagen der Bedienung und die Hauptfunktionen von **Nextcloud** erfahren sowie ein paar Informationen über grundlegende Dateioperationen und persönliche Einstellungen.
