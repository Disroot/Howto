---
title: Nube
subtitle: "Funcionamiento, configuraciones, clientes"
icon: fa-cloud
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - nube
        - nextcloud
page-toc:
    active: false
---

![](cloud.png)


El servicio **Nube de Disroot** nos permite hospedar y compartir nuestros archivos en línea. Tiene también varias funcionalidades tales como compartir calendario, gestión de contactos, tareas y notas, conferencia de audio/video, un tablero de proyecto básico, formularios y más.

Es el servicio medular de **Disroot** y la interfaz de usuarie más importante que intentamos integrar con la mayoría de las aplicaciones que ofrecemos.

Está desarrollado por **Nextcloud**, un software libre y de código abierto que nos permite subir y almacenar archivos a un servidor (podría ser el tuyo propio), y sincronizarlos con diferentes dispositivos a los que podemos acceder de manera segura desde cualquier lugar a través de internet.

En los siguientes capítulos de esta guía de la **Nube**, conoceremos la interfaz, los fundamentos de su operatoria y las principales características de **Nextcloud**, así como algunas acciones básicas sobre archivos y configuraciones personales.
