---
title: Cloud
subtitle: "Basics, settings, clients"
icon: fa-cloud
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - cloud
        - nextcloud
page-toc:
    active: false
---

![](cloud.png)

**Disroot's Cloud** service allows us to host and share our files online. It also have several online functionalities, such as calendar sharing, contacts, tasks and notes management, call/video conference, a basic project board, forms and more.

It is **Disroot**'s core service and the main user interface we try to integrate with most of the apps we offer.

It is powered by **Nextcloud**, a free and open source software that allows us to upload and storage files on a server (it could be a server of our own), and syncing them with different devices to which we can safely access from anywhere via internet.

In the following chapters of this **Cloud** howto, we will learn about the interface, the basics of its operation and the main features of **Nextcloud**, as well as some basic actions on files and personal settings.
