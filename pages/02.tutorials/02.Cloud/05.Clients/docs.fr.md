---
title: 'Clients'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - nextcloud
        - clients
page-toc:
  active: false
---

# Qu'est-ce qu'un client ?

Dans le contexte de l'informatique et des technologies de l'information (TI), un "**client**" est un logiciel ou un ordinateur qui interagit avec et utilise les ressources d'un autre ordinateur appelé "**serveur**".

Dans les guides de ce site, nous utiliserons ce terme pour désigner le premier, le logiciel.

Avec un client, vous pouvez accéder à certaines des principales fonctionnalités de **Disroot Cloud** (comme la messagerie, les calendriers, les contacts, les fichiers, les tâches, etc.) directement depuis votre ordinateur ou votre appareil mobile.

Comme il en existe beaucoup, nous essaierons de couvrir principalement ceux qui sont des **logiciels libres et open source** et qui, à notre avis, sont les plus complets.

Bien sûr, si vous utilisez un logiciel qui ne figure pas dans ces **Howtos** et que vous estimez qu'il devrait y figurer, vous pouvez collaborer avec la communauté en en rédigeant un vous-même (voir la section [Comment contribuer](/contribuer)).

---

# Clients Nextcloud

## [Clients de bureau et intégration](desktop)
- Comment configurer les clients de synchronisation **Nextcloud** et autres.
- Intégration des environnements de bureau sur GNU/Linux

## [Clients mobiles](mobile)
- Client mobile **Nextcloud
- **DAVx⁵**, paramètres de l'appareil
