---
title: Desktop
published: true
indexed:
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - desktop
        - clients
        - integration
visible: true
page-toc:
    active: false
---

# Desktop clients

## Multiplatform
- ### [Nextcloud client](multiplatform/nextcloud.client)

## GNU/Linux
- ### [GNOME desktop integration](gnu-linux/gnome)
- ### [KDE desktop integration](gnu-linux/kde)

## MacOS
- ### [MacOS device integration](mac-os)
