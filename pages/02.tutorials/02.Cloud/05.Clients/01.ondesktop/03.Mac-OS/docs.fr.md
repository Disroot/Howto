---
title: MacOS
visible: true
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - desktop
        - integration
        - macos
page-toc:
     active: false
---

# macOS Desktop clients

## [Synchronisation des calendriers](calendar-syncing)
## [Synchronisation des contacts](contact-syncing)
