---
title: MacOS
visible: true
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - desktop
        - integration
        - macos
page-toc:
     active: false
---

# macOS Desktop clients

## [Syncing Calendars](calendar-syncing)
## [Syncing Contacts](contact-syncing)
