---
title: 'GNOME Desktop integration'
published: true
indexed: true
visible: false
updated:
    last_modified: "March, 2024"
    app: "GNOME Desktop"
    app_version: 45.4 on Manjaro Linux
taxonomy:
    category:
        - docs
    tags:
        - sync
        - client
        - desktop
        - gnome
        - calendar
        - cloud
visible: false
page-toc:
    active: true
---

![](Gnomelogo.svg)

# GNOME Desktop integration
**GNOME** is a free and open source graphical desktop environment for **GNU/Linux** based operating systems. It has an excellent integration with **Disroot**'s cloud services.

In the context of desktop environments, integration is the ability to use a number of web services and applications directly on the desktop as if they were just computer programs. In the case of **GNOME**, moreover, this level of integration makes it look and feel unified with the rest of the applications.

Once we have set up our **Disroot** account with **GNOME** we will not need to configure anything else. On top of that, the process is pretty simple.

---

# Setting up an Online Account
When we setup an Online Account the cloud tasks, calendars and contacts will be automatically configured and accesible from the **Evolution** manager and the **GNOME Task**, **Contacts** and **Calendars** apps.

1. We open the **GNOME Settings** and go to **Online Accounts**

![](en/online.accounts_1.png)

 2. Select **Nextcloud** and fill in the required data:
    - **Server:** https://cloud.disroot.org
    - **Username:** *your_username*
    - **Password:** *your_super_secret_password*
    - Click on **Connect**

![](en/online.accounts_2.png)

If we entered the data correctly, a box will appear with the aspects that we can integrate. By default, they are all active, but we can activate/deactivate them as we wish.

![](en/online.accounts_3.png)

Now services such as the **Calendar**, **Tasks** and **Contacts** can be found and used directly on our desktop.

Next we will see how to work with the **Cloud** services integrated in the **GNOME** desktop.


## Calendar
The Calendar app is quite easy to use. From it we can manage our calendars and perform basic operations such as adding, editing and removing events. To do this we need to have installed the **GNOME Calendar** app.

!!! _It should be already installed and accessible from the **Software menu**. If not, we will have to search for it and download it from the system's software centre._

Once we have our account connected we will notice that our calendars are already integrated if we press on the **current time** in our top bar on the desktop.

![](en/calendar_sync.png)

The calendar window will automatically display all the events (if any) from our **Disroot**'s cloud account. We will, of course, receive all notifications on upcoming events as well.

The same will happen if we open the **GNOME Calendar app**.

![](en/calendar_1.png)

From now on we can synchronize and perform all the basic operations of managing our calendars in the cloud from the desktop.

## Tasks
The cloud tasks will be configured automatically and can be managed from, for example, the **Evolution** task manager.

![](en/tasks.png)

In the same way as our calendars, we can manage our tasks from the desktop and synchronize them through the cloud.

## Contacs
The configuration of our online account will also automatically integrate our cloud contacts (if any) from **Evolution** or the **Contacts** application.

![](en/contacts.png)