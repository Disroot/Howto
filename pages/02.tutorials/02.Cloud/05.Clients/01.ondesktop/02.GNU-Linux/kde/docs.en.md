---
title: 'KDE Plasma Desktop integration'
published: true
indexed: true
visible: false
updated:
    last_modified: "March, 2024"
    app: "KDE Plasma Desktop"
    app_version: 5.27.10 on Manjaro Linux
taxonomy:
    category:
        - docs
    tags:
        - sync
        - client
        - plasma
        - desktop
        - calendar
        - cloud
visible: false
page-toc:
    active: true
---

![](kde.logo.svg)

# KDE Plasma Desktop integration
**Plasma** is a free and open source desktop environment for **GNU/Linux** based operating systems.

In the context of desktop environments, integration is the ability to use a number of web services and applications directly on the desktop as if they were just computer programs.

Once we have set up our **Disroot** account with **Plasma** we will not need to configure anything else. On top of that, the process is pretty simple.

---

# Setting up an Online Account
When we setup an Online Account the cloud tasks, calendars and contacts will be automatically configured and accesible from the **KOrganizer** manager and the **Kontact** app.

1. We open the **Plasma Settings** and go to **Online Accounts** first and then click on **+ Add New Account**.

![](en/oa.new.png)

We select **Nextcloud**.

![](en/oa.nextcloud.png)

Then we enter the server address (`https://cloud.disroot.org`) and click on **Next**.

![](en/oa.nextcloud.server.png)

Now we will have to **Log in** to our account.

![](en/oa.login.png)

We enter our **Disroot** credentials and then click on **Log in**.

![](en/oa.login_credentials.png)

We click on **Grant access**.

![](en/oa.grant.access.png)

**Plasma** will then connect to our cloud and ask us which services we want to sync. We select them and click on **Finish**.

![](en/oa.services.png)

Now we have our desktop connected to the cloud and applications such as **Kontact** and **KOrganizer** (if we have them installed) will be automatically configured.

![](en/oa.added.png)

# Calendars, contacts and tasks
**Kontact** is the **Plasma** application to manage calendars, tasks, notes, contacts and even email. If we do not have it installed, we can download it from the **Software manager**.

![](en/kontact.png)

**Kontact** will already be set up with our online account information, and we will be able to access and manage our calendars,...

![](en/kontact_calendars.png)

... our contacts,...

![](en/kontact_contacts.png)

... and our tasks.

![](en/kontact_todo.png)

