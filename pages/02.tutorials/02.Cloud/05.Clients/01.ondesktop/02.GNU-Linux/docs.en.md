---
title: GNU/Linux
published: true
updated:
taxonomy:
    category:
            - docs
    tags:
            - cloud
            - desktop
            - integration
            - clients
            - gnu-linux
visible: true
page-toc:
     active: false
---

# GNU/Linux Desktop clients

## [GNOME Desktop Integration](gnome/)

## [KDE Desktop Integration](kde)

