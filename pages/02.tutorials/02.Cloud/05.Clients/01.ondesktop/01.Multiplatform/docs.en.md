---
title: Multiplatform
published: true
indexed:
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - clients
        - multiplatform
visible: true
page-toc:
    active: false
---

# Multiplatform Clients

## [Nextcloud client](nextcloud.client/)
- Desktop sync client
