---
title: Mobile
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - mobile
        - nextcloud
indexed: 
visible: true
page-toc:
     active: false
---

# Mobile Clients

## [Android](android)
- ### [Nextcloud app](android/nextcloudapp/) - Synchronisation and cloud files manager.
- ### [DAVx⁵](android/davx5/) - CalDAV/CardDAV management and synchronisation app.

## [iOS](ios)
- ### [iOS calendar sync](ios/calendar-syncing/)
- ### [iOS contacts sync](ios/contact-syncing/)