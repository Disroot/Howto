---
title: Android
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - android
        - nextcloud
visible: true
page-toc:
     active: false
---

# Android
## Nextcloud clients and integration

- ### [Nextcloud app](nextcloudapp)
  - Configuring and using the **Nextcloud** app with a **Disroot** account.

- ### [DAVx⁵](davx5)
  - How to sync Calendars, Contacts and Tasks.
