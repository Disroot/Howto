---
title: 'DAVx⁵'
published: true
visible: false
updated:
        last_modified: "March, 2024"
        app: DAVx⁵
        app_version: 4.3.13-ose on Android 11
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - davx5
        - clients
        - cloud
        - sync
        - android
page-toc:
    active: false
---

# DAVx⁵: CalDAV/CardDAV sync suite
**DAVx⁵** is a CalDAV/CardDAV management and synchronisation app for **Android** which natively integrates with **Android** calendar/contact apps.

It is recommended to install **DAVx⁵** from the [**F-droid**](https://f-droid.org/) app store. It is an installable catalogue of Free and Open Source software applications for the Android platform.

![](../en/fdroid.png)

After we have installed and setup **F-droid** we can search and install [DAVx⁵](https://f-droid.org/en/packages/at.bitfire.davdroid/).

![](en/davx_install.png)

# Configuring DAVx⁵
Once installed, we open the application.

![](en/davx_1.png)

Next screen will show us an option to enable or not *Regular sync intervals* and, depending on the device, it may displays compatibility suggestions to prevent device from blocking synchronization. We tap on the check mark icon.

![](en/davx_2.png)

To set up our **Disroot** account, we tap on the **+** (plus) button.

![](en/davx_new.account.png)

Then we select **Login with url and username**, and fill in with server address and credentials:

- **Server address**: `https://cloud.disroot.org`
- **Username**: `our Disroot username`
- **Password**: `our Disroot password`

Then **Login**.

![](en/davx_username.png)

Now **DAVx⁵** will try to detect our cloud configuration...

![](en/davx_detect.png)

... and if everything is OK, then we will be prompted to create the account.

![](en/davx_new.account_2.png)

Now we need to select
1. the addressbooks...

 ![](en/davx_select.addressbook.png)

2. ...and calendars we want to sync from **Disroot** to our **Android** device.

  ![](en/davx_select.calendars.png)

We can tap over the orange synchronization button to get the data available immediately.

And that's it. Now we can access our calendars, contacts and tasks in the cloud from any app of our choice.