---
title: 'Clients'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - nextcloud
        - clients
page-toc:
  active: false
---

# What is a client?

In the context of computing and Information Technologies (IT), a "**client**" is a software program or a computer that interacts with and uses resources of another computer that is called "**server**".

In the guides on this site, we will use the term to refer to the first one, the software.

With a client, we can access some of the **Disroot Cloud** main features (such as email, calendars, contacts, files, tasks, etc) directly from our computer or mobile device.

Since there are many, we will try to cover mainly those that are **Free and Open Source software** and, in our opinion, are the most complete.

Of course, if we use one that is not in these **Howtos** and we consider that it should be, we can collaborate with the community by writting one ourselves (see the [How to Contribute](/contribute) section).

---

# Nextcloud Clients

## [Desktop clients and Integration](ondesktop)
## 1. Multiplatform Nextcloud clients (GNU/Linux · Microsoft Windows · Apple macOS)

- ### [Nextcloud client](ondesktop/multiplatform/nextcloud.client/)

## 2. Desktop integration
Desktop integration refers to graphical environments that have built-in support for online services. These allow us to very simply set up our **Nextcloud** account and use a set of native applications seamlessly. To learn how to integrate the **Disroot** cloud with our desktop, we can follow any of the tutorials below.

- ### [GNOME Desktop Integration](ondesktop/gnu-linux/gnome/)

- ### [KDE Plasma Desktop Integration](ondesktop/gnu-linux/kde/)

---

## [Mobile clients](onmobile)

## [Android clients](onmobile/android/)
- ### [Nextcloud mobile app](onmobile/android/nextcloudapp/)

- ### [DAVx⁵](onmobile/android/davx5/)

## [iOS clients](onmobile/ios/)
- ### [iOS calendar sync](onmobile/ios/calendar-syncing/)
- ### [iOS contacts sync](onmobile/ios/contact-syncing/)