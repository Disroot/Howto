---
title: Interface
published: true
visible: true
indexed: true
updated:
        last_modified: "February, 2024"
        app: Nextcloud
        app_version: 27.10.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - nextcloud
        - interface
page-toc:
  active: true
---

# The User Interface (UI)

## The Dashboard

![](en/dashboard.png)

When we access the cloud, the **Dashboard** will welcome us. This give us an overview of our upcoming tasks and appointments, chat messages, incoming tickets, latest toots and much more. We can add the widgets we like.

![](en/dashboard.customize.png)


To do so, we just click on the **Customize** button and select the widgets we want to be displayed and we can even order them as we see fit by dragging and dropping them.

![](en/dashboard.mp4?resize=1024,576&loop)


OK. Let's start with an overview of the basic operation of the cloud.

## Overview

![](en/overview.png)

All the cloud applications follow this same basic structure with some slight variations.

### 1. The app navigation bar
In this bar we will find all the applications available on or integrated to the **Disroot** cloud, such as **Files**, **Tasks**, **Contacts** and **Notes** managers, **Calendar**, **Bookmarks**, **Photos**, **Talk**, **Deck** and the **Activity** log among others. It works as shortcuts to all the cloud applications and we can choose which of them we want to see in the bar (we will learn how to do it in the *Settings* chapter).

![](en/app.bar.gif)


### 2. The general functions
To the right of the navigation bar are:
  - the **Search** function where we can search anything within our cloud, files, tags, documents, comments, messages, notes, tasks, etc., and their related applications. We can also filter our searches by various categories to make them more precise.

    ![](en/search.gif)

  - the **Notifications** indicator that alerts us when we have messages, calls, comments on files, dates or appointments in our calendar, etc.;

  - the **Contacts** search to easily and quickly find any contact in our address book;

    ![](en/contacts.search.png)
  
  - and the **user menu** from where we can set our online status, access the appeareance configuration and the general settings, learn about Nextcloud, read their user manual or log us out.

    ![](en/user.menu.png)


### 3. Apps information section
When we select an app, its options, actions and functions will appear here.

  ![](en/apps.info.png)


### 4. Apps main view
This is the cloud main section, the workspace where files, contents and information of a selected app are displayed.

  ![](en/main.view.png)


### 5. The open/close apps information section button

  ![](en/open.close.gif)


Now we have an overall idea of the main parts of the interface. Before we continue to the cloud apps let's see our settings and how to configure them.