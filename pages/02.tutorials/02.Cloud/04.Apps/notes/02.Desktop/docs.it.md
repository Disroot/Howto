---
title: Desktop
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
        - desktop
page-toc:
    active: false
---
# Note Desktop client

Per utilizzare **Note** (creare, modificare e condividere note), controlla i seguenti tutorial: 

## [Nextcloud app](/tutorials/cloud/clients/desktop/multiplatform/desktop-sync-client)

#### [Sincronizzazione dei task con Thunderbird](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts#tasks-integration-with-with-thunderbird)

----
## How-To correlati

- [GNOME](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [KDE](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
