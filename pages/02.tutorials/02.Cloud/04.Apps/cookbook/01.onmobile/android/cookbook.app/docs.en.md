---
title: 'Nextcloud Cookbook'
published: true
visible: false
updated:
        last_modified: "March, 2024"
        app: Nextcloud Cookbook
        app_version: 3.0.2 on Android 11
taxonomy:
    category:
        - docs
    tags:
        - cookbook
        - clients
        - cloud
        - android
page-toc:
    active: true
---

# Nextcloud Cookbook app
This is another pretty simple and straightforward app. With it we can add, search for and view recipes and update and sync them with our **Disroot** cloud.

If we do not have it installed yet, it is recommended to do it from the [**F-droid**](https://f-droid.org/) app store. It is an installable catalogue of Free and Open Source software applications for the **Android** platform.

![](../../../../clients/onmobile/android/nextcloudapp/en/fdroid.png)

After we have installed and setup **F-droid** we can search the [Nextcloud Cookbook app](https://f-droid.org/en/packages/de.micmun.android.nextcloudcookbook/) and install it.

![](en/fdroid.install_1.png)

The first time we open the **Cookbook** app, it will ask us to choose whether to log in (in case we want to synchronize our mobile with our cloud) or skip to use the local storage. We will choose to **Login**.

![](en/cookbook_login.png)

If we have the [**Nexcloud app**](../../../../../clients/onmobile/android/nextcloudapp/) or [**DAVx⁵**](../../../../../clients/onmobile/android/davx5) installed then we will only need to choose the account we want to connect.

![](en/cookbook_account.png)

We grant **Cookbook** permission to access our account...

![](en/cookbook_allow.png)

... and then we need to choose whether or not to grant permissions to the app to access our media files.

![](en/cookbook_allow.app.png)

Once the app is connected, it will display all the recipes stored in the cloud (if any).

![](en/cookbook_connected.png)

# Using the Cookbook app
We click on a recipe and we will access to all the information contained in it.

![](en/cookbook_recipe.png)

We can read additional information (if any)...

![](en/cookbook_additional.info.png)

... the ingredients...

![](en/cookbook_ingredients.png)

... and the instructions.

![](en/cookbook_instructions.png)

## Importing a recipe
To add a recipe from our device, we tap the three-line menu at the top just before the search field, or slide the panel from the left side of the app, and select the **Import Recipe** option.

![](en/cookbook_import.opt.png)

In the next screen we paste the **Recipe URL** which, of course, we copied previously, and tap **Download**. The app will try to add it to our collection. Additionally, in this same screen we can change the target folder where recipes are saved.

![](en/cookbook_import.png)

# Application menu and settings
In the main screen we have the **App menu**, the **Search** and the **Sort** functions and the **Account** options.

![](en/cookbook_options.png)

## 01. App menu
We have already seen how to access the app menu: taping the three-line menu or sliding the panel from the left side of the app.

![](en/cookbook_menu.png)

### Advanced search
In addition to the **Search** function on the main screen, we have the **Advanced Search** option. It allows us to perform more narrow searches.

![](en/cookbook_adv.search.png)

### Categories
For quicker access to our recipes by categories.

### App
In addition to the **Import Recipe** we saw previously, there are the **Settings**.

![](en/cookbook_settings.png)

We can modify the **Recipe Directory**, set the **Synchronization** interval and connection conditions, change the app **Theme** and override the lock screen function when we are reading a recipe.

## 02. Search function
For a basic search.

## 03. Sort function
To organize our recipes according to several criteria.

![](en/cookbook_sort.png)

## 04. Account options
We can switch between accounts if we have more than one configured, or **Add a new account**.

![](en/cookbook_account.opt.png)

---

Time to cook!