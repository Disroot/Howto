---
title: Cookbook
published: true
visible: false
indexed: true
updated:
        last_modified: "March, 2024"
        app: Nextcloud Contacts
        app_version: 27.10.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - cookbook
visible: true
page-toc:
    active: true
---

![](cookbook.svg)

# Cookbook

![](en/cookbook.png)

This app is a library for our recipes. We can manually add a recipe to the collection, or we can paste its URL into the recipe, and the provided web page will be parsed and downloaded to whichever folder we specify in the app settings.

---

# Create a recipe
To create a new recipe, we click on the **+ Create recipe** button.

![](en/cookbook_recipe.create.png)

We then add a title and any useful information. If a block of information is empty, it will not be displayed in **Cookbook**.

![](en/cookbook_recipe.creating.png)

To add an image to the recipe, we have two ways of doing it.

- **Select an image previously stored on our cloud**: we click on the icon on the right of the image field and select the image. This way, the image will be copied in the recipe’s directory.

- **Load an image from an URL**: we just type or paste the URL in the field. The cookbook app will download and use the image.

We can also add a link to other recipes in the Description, Tools, Ingredients, and Instructions fields:

- we only need to type a `#` and select, in the popup list, the recipe to be linked.

![](en/cookbook_recipe.creating_link.png)

Also **Categories** and **keywords** are allowed. **Categories** (1) can be accessed more directly than **keywords** (2) , as they appear in the sidebar.

![](en/cookbook_categories.keywords.png)

By clicking a category in the sidebar, we can quickly narrow down recipes to a certain class, like “Main course” or “Pastries”. Then, keywords can be used to further narrow down the selection with tags like “vegetarian” or “easy”. In this way, categories are used for rough filtering and keywords are used for fine filtering.

Once we are done, click on **Save**.

![](en/cookbook_recipe.created.png)

If we need to edit a recipe, we just click on the **Edit** button at the top right corner.

![](en/cookbook_options.png)

The three-dots menu right next to the **Edit** button allows us to **Reload**, **Print**, **Clone** and **Delete** a recipe.

## Import a recipe
We can import a recipe from a website by entering its URL in the text-input field below the **+ Create recipe** button.

![](en/cookbook_add.url.png)

The **Cookbook** app requires the recipe site to support the [schema.org recipe standard’s](https://www.schema.org/Recipe) JSON+LD meta data. So not all websites recipes will be correctly displayed. In some cases, they could not even been downloaded/displayed.

![](en/cookbook_url.added.png)

# Cookbook settings
Clicking on **Cookbook settings** at the bottom of the side panel we access some options and configurations.

![](en/cookbook_settings.png)

## Sharing a recipe
Currently, the only way to share recipes is by sharing the cloud folder that contains all recipes with another **Nextcloud** user. To this end, the folder first needs to be shared from the cloud [**Files**](../../files/) app. Afterwards it can be set as the recipe directory in the **Cookbook settings** section.

---

# Cookbook mobile app
Although it is not necessary, we can install **DAVx5**, a CalDAV/CardDAV sync suite that will make the configuration easier in case of the **Nextcloud Cookbook** app. Click [**here**](../../clients/onmobile/android/davx5/) to learn how to configure it.

## [Nextcloud Cookbook app](onmobile/android/cookbook.app/)

