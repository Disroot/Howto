---
title: 'On desktop'
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Calendar: desktop clients and integration
A "desktop client" is an application that runs in our desktop or laptop computer. And although there are many clients for different operating systems, we are going to focus mainly on those that are Free and Open Source Software.

## 1. Multiplatform calendar clients (GNU/Linux · Microsoft Windows · Apple macOS)
### [Mozilla Thunderbird](thunderbird)
- Mail, calendar, contacts and tasks management

## 2. GNU/Linux clients
### [Evolution](evolution/)
- Mail, calendar, contacts and tasks management

## 3. Desktop integration
Desktop integration refers to graphical environments that have built-in support for online services. These allow us to very simply set up our Nextcloud account and use a set of native applications seamlessly. To learn how to integrate the **Disroot** cloud with our desktop, we can follow any of the tutorials below.

### [GNOME Desktop Integration](../../../05.Clients/01.ondesktop/02.GNU-Linux/gnome/)
- Calendar, Contacts and Tasks

### [KDE Plasma Desktop Integration](../../../05.Clients/01.ondesktop/02.GNU-Linux/kde/)
- KOrganizer, Kalendar and KAddressBook

