---
title: Desktop
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

# Client desktop per calendario

## [Thunderbird](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)
- Gestisci i tuoi calendari, contatti e task

## [calcurse](/tutorials/cloud/clients/desktop/multiplatform/calcurse-caldav)
- Gestisci i tuoi calendari da riga di comando

---

Guide correlate:

- [Integrazione con GNOME Desktop](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
- [Integrazione con KDE Desktop](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
