---
title: 'Thunderbird'
published: true
indexed: true
visible: false
updated:
    last_modified: "February, 2024"
    app: "Mozilla Thunderbird"
    app_version: 115.7.0 on Manjaro Linux
taxonomy:
    category:
        - docs
    tags:
        - sync
        - client
        - desktop
        - calendar
        - cloud
visible: false
page-toc:
    active: true
---

# Mozilla Thunderbird Calendar manager

![](thunderbird.logo.svg)

If we do not have Thunderbird installed, we can do it from [**this page**](https://www.thunderbird.net/en-US/thunderbird/all/) or from the software manager of our system if we are using GNU/Linux.

## Configuring our Disroot calendar
If we have never used Thunderbird before, the first time we open it we will be prompted to set up an account.

![](en/account.setup.png)

We enter our name, our Disroot username and password, check *Remember password* if we do not want Thunderbird to ask for it again and click *Enter*.

It will automatically fetch the server information and configure the account for us.

![](en/account.config.png)

We click on **Done** and that's it.

![](en/account.created.png)

Thunderbird then will look up the calendars, contacts and tasks we have configured in the cloud. We click on **Finish** and we will be able to use the calendars.

## Configuring a new calendar
If we want to create a new calendar we click on the **Calendar** button in the application main screen.

![](en/new.calendar.png)

The procedure is similar to the previous one. We select **On the network** option and then **Next**.

![](en/create.new.calendar.png)

We enter our Disroot username and the location of the calendar which is the cloud URL and then **Find Calendars**.

![](en/create.new.calendar_2.png)

Thunderbird will ask for our Disroot password. We enter it, check the option *Use Password Manager to remember this password* if we do not want the application to ask for it again and click on **OK**

![](en/create.new.calendar_3.png)

In the next step the calendars that we have configured (if any) will appear and we will be able to select them to subscribe and synchronize. We will also be able to edit some calendar options by clicking in its **Properties**.

![](en/create.new.calendar_4.png)

Once we are ready, we click on **OK**...

![](en/create.new.calendar_5.png)

... and, again, that's it. We will have our calendars configured to work with.

![](en/new.calendar.created.png)


## Creating a new event
Creating an event is as simple as double-clicking on a day or opening the context menu and selecting **New event** and filling in the iformation we consider important and finally click on the **Save and close** button.

![](en/new.event.gif)

If we check our [**Calendar app**](../../) in the **Cloud** we will find the event and we can edit it if necessary.