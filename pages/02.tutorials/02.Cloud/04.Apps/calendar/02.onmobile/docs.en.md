---
title: 'On mobile'
published: true
visible: false
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - clients
        - cloud
        - sync
        - android
page-toc:
    active: false
---

# Calendar mobile clients
In order to setup and use **Disroot** calendars on our device, we need to have previously installed **DAVx5**, a CalDAV/CardDAV sync suite. Click [**here**](../../../05.Clients/02.onmobile/01.android/01.davx5/) to learn how to configure it.


# Android clients

## [Fossify Calendar](android/fossify/)
