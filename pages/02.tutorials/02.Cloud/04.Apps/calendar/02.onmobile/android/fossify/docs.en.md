---
title: 'Fossify Calendar'
published: true
visible: false
updated:
        last_modified: "March, 2024"
        app: Fossify Calendar
        app_version: 1.0.2 on Android 11
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - davx5
        - clients
        - cloud
        - sync
        - android
page-toc:
    active: true
---

# Fossify Calendar
It is a free and open source software application to manage our calendars. To install the app, it is recommended to do it from the [**F-droid**](https://f-droid.org/) app store. It is an installable catalogue of Free and Open Source software applications for the **Android** platform.

![](../../../../../05.Clients/02.onmobile/01.android/02.nextcloudapp/en/fdroid.png)

After we have installed and setup **F-droid** we can search and install the [Fossify Calendar app](https://f-droid.org/es/packages/org.fossify.calendar/).

![](en/fossify.fdroid.png)

## Configuring the app
In order to be able to access and manage our calendars from the app, we need to select them first. To do that, we open the app and tap the gear icon.

![](en/fossify.png)

Then we select **Settings**...

![](en/fossify_settings.png)

... and look for the **CALDAV** option. Tap on **CalDAV sync**.

![](en/fossify_settings.caldav.png)

In the **Select calendars to sync** screen option, we should see our cloud calendars. We select the ones we want to sync and tap **OK**.

![](en/fossify_settings.select.png)

And that's it. We should now have our calendars synced in our app.

![](en/fossify_synced.png)

## Creating new events and tasks
By tapping the **+** (plus) icon, we can add

![](en/fossify_add.png)

- **new tasks**
  
  ![](en/fossify_task.png)

- and/or **new events**

  ![](en/fossify_event.png)

Both of these operations work in a very similar way as in the web interface, so we should have no problems with the mechanics of their use.