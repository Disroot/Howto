---
title: Desktop
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - contacts
page-toc:
    active: false
---

# Integrazione con il desktop

Puoi leggere i tutorial di seguito per sincronizzare i tuoi **Contatti** tramite un client desktop multipiattaforma.

- [Thunderbird: sincronizzazione calendario/contatti/attività](/tutorials/cloud/clients/desktop/multipiattaforma/thunderbird-calendar-contacts)

In alternativa, puoi utilizzare e configurare l'integrazione desktop. 
 - [GNOME](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
 - [KDE](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
