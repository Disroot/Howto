---
title: "Contacts: Web"
published: true
indexed: true
visible: false
updated:
        last_modified: "February 2022"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - contacts
page-toc:
     active: true
---

# Contatti
## Crea un contatto

Nell'app dei contatti seleziona "*Nuovo contatto*"

![](en/contacts_add1.png)

Ti verrà chiesto di creare il nuovo contatto.

![](en/contacts_add2.png)

Basta digitare le informazioni che vuoi/hai nei campi. Se necessario, puoi semplicemente aggiungere più campi nella parte inferiore del modulo.

![](en/contacts_add3.png)


## Elimina un contatto

* Seleziona il contatto
* Nell'intestazione del modulo di contatto, seleziona l'icona Elimina

![](en/contacts_delete.png)

## Crea gruppi di contatti
Puoi creare gruppi per organizzare i tuoi contatti es.: docenti, lavoro, collettivo, ecc.
Nel campo gruppo puoi assegnare un nuovo contatto a un gruppo esistente o crearne uno nuovo. Oppure assegna un contatto a più gruppi digitando i vari gruppi. 
![](en/contacts_groups1.png)

Sul lato sinistro dello schermo nell'app dei contatti, vedrai i gruppi esistenti.
Selezionandoli verranno presentati tutti i contatti in quel rispettivo gruppo.

![](en/contacts_groups2.png)


## Condividi le rubriche

Vai su "impostazioni" nell'angolo in basso a sinistra dello schermo nell'app dei contatti.

![](en/contacts_share1.png)

Nelle impostazioni puoi condividere la tua rubrica con altri utenti **Disroot** tramite:<br>
 - Selezionando condividi rubrica
 - Scrivere il nome utente dell'utente **Disroot** con cui vuoi condividere la rubrica.

![](en/contacts_share2.png)

Puoi anche utilizzare un link per condividere la tua rubrica tramite **webDAV**, ad altre rubriche di contatti (**Thunderbird**, cellulare, ecc.).

![](en/contacts_share3.png)


## Importa rubriche

È possibile importare rubriche o contatti individuali, se si dispone di un file vcf del contatto o della rubrica.

* Selezionando "Importa".

![](en/contacts_import1.png)

Quindi seleziona il file che desideri importare e premi ok.


## Crea una nuova rubrica

All'interno delle impostazioni nel campo "Nome rubrica" ​​scrivere il nome della nuova rubrica, quindi premere invio.

![](en/contacts_create1.png) 

# Sincronizzazione dei contatti cloud con la webmail
Sincronizzare i contatti cloud con Webmail è molto semplice. Consentirà ai contatti dalla tua webmail e dal cloud di essere sincronizzati.

Per prima cosa vai alla tua app dei contatti **Nexcloud**. Fare clic sull'icona delle impostazioni nell'angolo in basso a sinistra.
Seleziona l'opzione "Copia collegamento" della rubrica che desideri sincronizzare con la webmail, l'indirizzo verrà copiato negli appunti.

![](en/webmail_contact_export.png)


Ora vai all'app **Mail** e fai clic sull'icona delle impostazioni (in alto a destra nell'app webmail)

![](en/webmail_contact_export_2.png)

Nelle tue impostazioni, nella barra laterale sinistra seleziona **Contatti** e una volta lì:

   1. Selezionare Abilita sincronizzazione remota
   2. In URL rubrica, incolla l'URL dalla rubrica dei contatti Cloud che hai salvato in precedenza.
   3. Fornisci il tuo nome utente
   4. Aggiungi la tua password

![](it/webmail_contact_export_3.png)

E quindi aggiorna entrambe le pagine. Ora i tuoi contatti rimarranno sincronizzati. 
