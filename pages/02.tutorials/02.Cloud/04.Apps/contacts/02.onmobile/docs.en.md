---
title: Mobile
published: true
visible: false
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - contacts
page-toc:
    active: false
---

# Contacts mobile clients
In order to setup and sync **Disroot** contacts on our **Android** device, we need to have installed **DAVx⁵**, a CalDAV/CardDAV sync suite.

## [DAVx⁵ tutorial](../../../05.Clients/02.onmobile/01.android/01.davx5/)
