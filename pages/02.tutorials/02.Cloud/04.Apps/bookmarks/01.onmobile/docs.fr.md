---
title: Calendrier : Portable
published: true
visible: false
updated:
        last_modified: "Juillet 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - calendrier
        - cloud
        - sync
page-toc:
    active: false
---

# Clients mobiles de l'application Calendrier

L'application Calendrier de **Disroot** est activée.

Pour configurer et utiliser les calendriers **Disroot** sur votre appareil, consultez le tutoriel suivant :

### [Nextcloud : Mobile Clients](/tutorials/cloud/clients/mobile)
