---
title: 'On mobile'
published: true
visible: false
taxonomy:
    category:
        - docs
    tags:
        - bookmarks
        - clients
        - cloud
        - sync
        - android
page-toc:
    active: false
---

# Bookmarks mobile clients
In the following chapters we will see how to sync and manage our bookmarks hosted in the cloud with our device. Although it is not necessary, we can install **DAVx5**, a CalDAV/CardDAV sync suite that will make the configuration easier in case we choose to use the **Nextcloud Bookmarks** app. Click [**here**](../../../05.Clients/02.onmobile/01.android/01.davx5/) to learn how to configure it.


## On Android
### [Nextcloud Bookmark app](android/bookmarks.app/)

### [floccus bookmark sync app](android/floccus/)

