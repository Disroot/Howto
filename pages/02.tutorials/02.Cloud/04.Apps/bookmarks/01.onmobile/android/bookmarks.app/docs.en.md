---
title: 'Nextcloud Bookmarks'
published: true
visible: false
updated:
        last_modified: "March, 2024"
        app: Nextcloud Bookmarks
        app_version: 1.24 on Android 11
taxonomy:
    category:
        - docs
    tags:
        - calendar
        - davx5
        - clients
        - cloud
        - sync
        - android
page-toc:
    active: true
---

# Nextcloud Bookmarks app
This is a pretty simple and straightforward app. With it we can add, edit, delete and view bookmarks and sync them with our **Disroot** cloud.

If we do not have it installed yet, it is recommended to do it from the [**F-droid**](https://f-droid.org/) app store. It is an installable catalogue of Free and Open Source software applications for the **Android** platform.

![](../../../../../05.Clients/02.onmobile/01.android/02.nextcloudapp/en/fdroid.png)

After we have installed and setup **F-droid** we can search the [Nexcloud Bookmarks app](https://f-droid.org/en/packages/org.schabi.nxbookmarks/) and install it.

![](en/fdroid.install_1.png)

# Setting up our Disroot account
Once the app is installed, we open it and tap **NEXTCLOUD SINGLE SIGN ON (SSO)**.

![](en/bookmark.app_sso.png)

If we have installed and configured [**DAVx5**](../../../../../05.Clients/02.onmobile/01.android/01.davx5/) the app will ask us to choose the account with which we want to log in. Otherwise we will need to select **Add account** and follow the steps described in the **Nextcloud** app setup [**here**](../../../../../05.Clients/02.onmobile/01.android/02.nextcloudapp#setting-up-our-disroot-ac).

![](en/bookmark.app_account.png)

Then the app will ask us if allow it to access our account...

![](en/bookmark.app_allow.png)

... and once it has connected to our cloud we will see our bookmarks.

![](en/bookmark.app_connected.png)

# Using the Bookmarks app
As we mentioned at the beginning, the app is pretty straightforward.

![](en/bookmark.app_interface.png)

## 1. Settings menu

  ![](en/bookmark.app_settings.menu.png)

Here we can find our settings, which are only two: the default tags for adding-sharing bookmarks.

  ![](en/bookmark.app_settings.png)

We can also see our **Tags** (if any) and read the **About** section.

## 2. Search
To find a bookmark by its name or tag.

## 3. Account management
Here we can select another previously configured cloud account or add a new one.

  ![](en/bookmark.app_add.acc.png)

## 4. Add bookmark button

  ![](en/bookmark.app_add.png)

We tap this button to add a new bookmark by entering its URL, giving it a title and optionally adding a description and tags.

  ![](en/bookmark.app_tags_added.png)

## Editing and sharing bookmarks
By doing a long press on a bookmark we will access the options menu from where we can:

![](en/bookmark.app_options.png)

- **Share** the bookmark through any of the apps installed in our device that allow sharing.
- **Edit** the bookmark URL, title, description and tags.
- **Delete** a bookmark.

And that's it.