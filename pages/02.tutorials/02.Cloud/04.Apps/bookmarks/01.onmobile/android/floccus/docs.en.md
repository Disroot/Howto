---
title: 'floccus bookmark sync'
published: true
indexed: true
visible: false
updated:
    last_modified: "March, 2024"
    app: "floccus bookmark sync"
    app_version: 5.0.9 on Android 11
taxonomy:
    category:
        - docs
    tags:
        - sync
        - client
        - desktop
        - bookmarks
        - cloud
visible: false
page-toc:
    active: true
---

# floccus bookmark sync
**floccus** is an open source syncing software which allows us to synchronize our bookmarks across all your devices and browsers.

If we do not have it installed yet, it is recommended to do it from the [**F-droid**](https://f-droid.org/) app store. It is an installable catalogue of Free and Open Source software applications for the **Android** platform.

![](../../../../../05.Clients/02.onmobile/01.android/02.nextcloudapp/en/fdroid.png)

After we have installed and setup **F-droid** we can search the [floccus bookmark sync](https://f-droid.org/en/packages/org.handmadeideas.floccus/) and install it.

![](en/floccus_app.png)

# Configuring floccus bookmarks

Once we have installed it, we open it and the configuration process will start. We will select **Nextcloud Bookmarks** option and then continue.

![](en/floccus_how.png)

In the next screen we enter the **Disroot** server address (`https://cloud.disroot.org`) then tap on **Connect**.

![](en/floccus_server.png)

The app will check the URL and ask us to log in so we can grant it access to our account.

![](en/floccus_signin.png)

We tap on **Log in**.

![](en/floccus_connect.png)

The we need to enter our **Disroot** credentials, tap on **Log in** again and finally **Grant access**.

![](en/floccus_grant.access.png)

In the next screen we will be ask to select which folder we want to sync. Unless we have previously configured another folder in the cloud for our bookmarks (in which case we will need to enter its path in the **Server target** field), we will leave this field empty to synchronize with the default directory. We tap **Continue**.

![](en/floccus_target.png)

The next screen will present us with a series of options related to the **Synchronization interval** and how we want to manage the changes made on the different devices we use. Once we are done, we tap on **Continue**...

![](en/floccus_sync.png)

... and the profile will be created.

![](en/floccus_profile.png)

Now **floccus** will fecth our bookmarks data.

![](en/floccus_fetch.png)

Depending on the amount of bookmarks and our internet connection, it will take just a moment to sync them all.

![](en/floccus_synced.png)

# floccus interface
The interface and use of the app is pretty intuitive.

![](en/floccus_interface.png)

## 1. Settings menu

![](en/floccus_settings.png)

Here we can
- add a **New Profile**, this process is the same as the initial profile setup;
- **Import/Export Profiles**

  ![](en/floccus_export.png)

## 2. Search bookmarks
While typing the name, the app will search all the bookmarks related to the term we are searching for.

## 3. Force sync
In addition to the time we set as the interval for automatic synchronization, we can force it at any time we want by tapping this button.

## 4. Sort by
We can sort our bookmarks by **Title**, **Link** or **Custom** order.

![](en/floccus_sort.png)

## 5. Options
Through these options we can set or modify:
- the **Server details**;
- the **Folder mapping**;
- the **Mobile settings**;
- the **Sync behavior**;
- and perform **Dangerous actions** such as download logs, reset the cache, enable o disable the failsafe mode and remove the current profile.

![](en/floccus_opt.details.png?lightbox)

## 6. Folder options
Tapping the three-dots menu we can

![](en/floccus_folder.png)

- **Edit** the folder name or its parent folder;

  ![](en/floccus_folder.edit.png)

- and/or **Delete** it.

## 7. Add options
By tapping this button we can

![](en/floccus_add.png)

- **Import bookmarks**;

  ![](en/floccus_import.png)

- **Add a bookmark**;

  ![](en/floccus_bookmark.add.png)

- or **Add a folder**.

  ![](en/floccus_add.folder.png)

# Editing and sharing a bookmark
By tipping the three-dots menu at the right of a bookmark name we can

![](en/floccus_bookmark.opt.png)

- **Edit** it;

  ![](en/floccus_bookmark.edit.png)

- **Share** it, through any of the apps that we have installed on our mobile and allow it;

- and/or **Delete** the bookmark.

That's it. Now we can have our bookmarks synchronized and manage them from the mobile.