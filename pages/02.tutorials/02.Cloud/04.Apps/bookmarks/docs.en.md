---
title: Bookmarks
published: true
visible: false
indexed: true
updated:
        last_modified: "March, 2024"
        app: Nextcloud Bookmarks
        app_version: 27.10.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - bookmarks
visible: true
page-toc:
    active: true
---

![](bookmarks.svg)

# Bookmarks
**Bookmarks** is an application for collecting, organizing and sharing our bookmarks. We can browse and filter our bookmarks via tags, folders and by using the built-in search feature. An we can also share folders with other users and groups as well as create public links for them.

Although its use is quite simple and intuitive, we will see its basic features to get to know it better.

---

# Interface
The first time we open our **Bookmarks** app, it will look like this:

![](en/bookmarks.png)

Right away we can see three options in the main window that are self-explanatory:

- **Add a bookmark**
- **Import bookmarks**
- **Sync with your browser**

On the left sidebar we have the filters and the **Settings** at the bottom.

- **Recent**: it will show us the last bookmarks we have added or modified.
- **Shared with you**: we can share bookmark folders with others. All folders shared with us will be listed here.
- **Files**: all bookmarks to files (like photos or documents) will automatically be saved to our Nextcloud files, so we can still find them even if the link goes offline.
- **Duplicated**: a bookmark can be in multiple folders at once. Updating it will update all copies. All duplicated bookmarks are listed here so we can decide what to do with them.
- **Broken links**: bookmarked links are checked regularly and the ones that cannot be reached are listed here.
- **New tag**: we can add tags to our bookmarks for a better organization. This is a practical way to create new ones.
- **Search tags**: to find bookmarks more quickly through their tags.

At the top we have
- the button to create a **New bookmark** or a **New folder**
- the **Search field**, to search by names, tags or urls;
- the **RSS feed** to copy and share;
- the **Sort** function, to sort according to different criteria;
- and the **Toggle** button to change the view from list to grid and vice versa.

![](en/topbar.gif)

## Settings

![](en/settings.png)

- **Import**: to import bookmarks if we have a copy of them previously saved somewhere on our computer.
- **Export**: to export our bookmarks to a HTML file.
- **Archive path**: by default, the path to the directory where bookmarks are saved is **/Bookmarks**, but we can change it to a different one within the cloud directories.
- **Backups** by enabling it, the application will make regular backups and save them in the default directory or in another directory of our choice.
- **Client apps**: we can check out a list of client apps that integrate with this application by clicking on the link: [**Client apps**](https://github.com/nextcloud/bookmarks#third-party-clients)
- **Install web app on device**: to install the app on our device home screen.
- **Bookmarklet**: by dragging the button to our browser bookmarks we can quickly bookmark any webpage.

  ![](en/bookmarklet.gif)

## Adding a bookmark
To save a bookmark in the application, we just click on the **+** (plus) icon and then on **New bookmark**.

![](en/add.gif)

We will see a field to enter the URL of the site we want to save. Then we click on the check icon and the site will be saved.

## Bookmarks options
Once we have added a bookmark we can perform several operations on it by clicking on the three-dots at the right of its name.

![](en/options.png)

- **Details**: here we can rename, modify the link, add tags and a description of the bookmark.

  ![](en/details.gif)

- **Select bookmark**
- **Rename**
- **Copy link**: to copy the bookmark URL to the clipboard.
- **Move**: to move the bookmark, e.g. to another directory. We can also drag and drop it.
- **Add to folders**: to add the bookmark to another folder.
- **Delete**: to remove the bookmark.

## Folders options
Similar to bookmarks, we can perform different operations with folders.

![](en/folder.details.gif)

We can
- see its **Details**, like the owner of the folder and the number of bookmarks that it contains. Also from this option we can share the folder through a link or with other users, and delete it too.
- **Select the folder**
- **Share the folder**
- **Rename folder**
- **Move the folder**
- and **Delete the folder**

And that's it. Pretty simple and useful.

---
# Bookmarks app clients
There are also mobile clients for the cloud **Bookmarks** app. To learn how to configure and use them, we can read the following tutorials.

## [On mobile](onmobile)