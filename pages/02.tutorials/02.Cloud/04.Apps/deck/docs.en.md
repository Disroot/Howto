---
title: Deck
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Deck
        app_version: 1.2.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - deck
visible: true
page-toc:
    active: false
---

![](deck.svg)

# Deck (coming soon)

**Deck** is a kanban style organization tool aimed at personal planning and project organization for teams integrated with your **Disroot Cloud**.
