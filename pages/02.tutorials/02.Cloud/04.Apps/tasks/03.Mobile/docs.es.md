---
title: "Tareas: Móvil"
published: true
visible: false
updated:
        last_modified: "Julio 2019"
taxonomy:
    category:
        - docs
    tags:
        - tareas
        - nube
        - móviles
page-toc:
    active: false
---

## Integración de Tareas en dispositivos móviles

Para configurar y sincronizar tus **Tareas** a través de un cliente para móviles, mira los tutoriales a continuación:

### Android
- [DAVx⁵ / OpenTasks](/tutorials/cloud/clients/mobile/android/calendars-contacts-and-tasks)
- [Aplicación móvil de Nextcloud](/tutorials/cloud/clients/mobile/android/nextcloud-app)
