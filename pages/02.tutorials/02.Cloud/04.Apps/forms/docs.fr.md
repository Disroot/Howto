---
title: Formulaires
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Formulaires
        app_version: 2.1.0
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - formulaires
visible: true
page-toc:
    active: false
---

# Formulaires (à venir)

**Formulaires** fournit des enquêtes et des questionnaires simples.
