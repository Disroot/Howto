---
title: Forms
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Forms
        app_version: 2.1.0
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - forms
visible: true
page-toc:
    active: false
---

![](forms.svg)

# Forms (coming soon)

**Forms** provides simple surveys and questionnaires.
