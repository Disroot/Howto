---
title: Rendez-vous
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Appointments
        app_version: 1.7.15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - applis
        - Rendez-vous
visible: true
page-toc:
    active: false
---

# Rendez-vous (bientôt disponible)

Réservez des rendez-vous dans votre **Calendrier Disroot** via un formulaire en ligne sécurisé. Les participants peuvent confirmer ou annuler leurs rendez-vous via un lien e-mail.
