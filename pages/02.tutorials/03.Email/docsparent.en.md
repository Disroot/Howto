---
title: Email
subtitle: "Settings, clients."
icon: fa-envelope
published: true
visible: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - email
page-toc:
    active: false
---

![](email.png)

**Disroot Email** provides secure email service that can be used through email clients on different devices or from our web client.  
Here we can find the settings, configurations and tutorials related to this service.