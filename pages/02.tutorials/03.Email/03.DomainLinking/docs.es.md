---
title: 'Solicitud de enlace de dominio'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - cuenta
        - domain linking
page-toc:
    active: false
---

# How to request domain linking

**Disroot** offers a custom domain feature for email. Get more information on our [website](https://disroot.org/en/perks).