---
title: 'Clients de bureau'
published: true
visible: true
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - bureau
page-toc:
    active: false
---

![](../thumb.png)

## Clients de messagerie multiplatformes
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)


## GNU/Linux
- [Mutt](mutt)
- [Emacs-Mu4e](emacs-mu4e)


## GNU/Linux: Intégration email du bureau
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)
