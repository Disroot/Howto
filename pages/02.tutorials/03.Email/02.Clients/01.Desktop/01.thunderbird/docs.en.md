---
title: Thunderbird
visible: false
published: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Mozilla Thunderbird

(tb_logo.png)
**Thunderbird** est un puissant client de messagerie, calendrier et gestionnaire de flux RSS open-source. Vous pouvez gérer tous vos comptes de messagerie, calendriers, flux de nouvelles, tâches et même chat à partir d'un seul endroit.

# Installing Thunderbird
If you don't have it installed yet, go to [Thunderbird page](https://www.thunderbird.net/) and choose your language and Operating System.

!! **NOTE**<br>
!! Most **GNU/Linux** distributions usually have the latest and updatable version of **Thunderbird**. We recommend using your distribution package manager to install it.

----

## [Setting up your Disroot account](setup)

## [Exporting / Importing emails](exporting)
