---
title: 'Desktop Clients'
published: true
visible: true
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

![](../thumb.png)

## Client di posta multipiattaforma
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)


## GNU/Linux
- [Mutt](mutt)
- [Emacs-Mu4e](emacs-mu4e)

## GNU/Linux: integrazione col desktop
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)
