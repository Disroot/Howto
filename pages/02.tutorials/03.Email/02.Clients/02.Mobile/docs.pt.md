---
title: 'Clientes de telemóvel'
visible: true
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Clientes de telemóvel


Como configurar o seu email Disroot no seu telemóvel:

## Table of content
 - [K9 - Aplicação de email para Android](androidk9)
 - [SailfishOS](sailfishos)
 - [iOS](ios)

![](mobile.jpg)
