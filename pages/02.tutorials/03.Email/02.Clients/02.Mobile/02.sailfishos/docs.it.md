---
title: Mobile clients: SailfishOS mail client
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Email con SailfishOS

Configurare la tua posta **Disroot** su SailfishOS è molto semplice. Basta seguire questi semplici passaggi. (ci è voluto più tempo per fare quegli screenshot fantasiosi :P)


1. Apri le **Impostazioni**

2. Vai fino in fondo (Sailfish2.0) alla scheda **Account**

3. Seleziona **Email generale**

![](en/sailfish_mail1.png)

4. Inserisci il tuo indirizzo e-mail **disroot** e la password e clicca su "Accetta".

![](en/sailfish_mail2.png)

5. Impostazioni del server.

  - **Server di posta in arrivo:**
     - Modifica il nome utente e **rimuovi il dominio lasciando solo il nome utente**
     - Aggiungi l'indirizzo del server: **disroot.org**
     - **Abilita connessione SSL**
	 
![](en/sailfish_mail3.png)

- **Server di posta in uscita:**
     - Indirizzo del server: **disroot.org**
     - Connessione sicura: **StartTLS**
     - Porto: **587**
     - **Autenticazione richiesta**
	 
![](en/sailfish_mail4.png)

6. Clicca su "**Accetta**"

7. Modifica dettagli come **descrizione** e "**Il tuo nome**" e clicca su "**Accetta**"

![](en/sailfish_mail5.png)

**Fatto!** \o/
