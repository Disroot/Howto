---
title: "Clients de courriel"
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

![](thumb.png)

## Clients de courriel du bureau
- [**Multi platformes**](desktop)
- [Intégration du bureau **GNOME**](desktop/gnome-desktop-integration)
- [Intégration du bureau **KDE**](desktop/kde-desktop-integration)


## Clients de courriel pour mobiles
- [**Android: FairEmail**](mobile/fairemail)
- [**Android: K9**](mobile/k9)
- [**SailfishOS: Mail App**](mobile/sailfishos)
- [**iOS: Mail App**](mobile/ios)
