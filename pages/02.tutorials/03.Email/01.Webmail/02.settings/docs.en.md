---
title: "Settings"
published: true
visible: true
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Table of Content
## [01. Preferences](01.preferences)
  - User interface
  - Mailbox view
  - Displaying messages
  - Composing messages
  - Contacts
  - Special folders
  - Server settings
  - Deleting old messages
  - Message highlights

## [02. Folders](02.folders)
## [03. Identities](03.identities)
  - Default identity
  - Add other identities / aliases
  - Sending an email with another identity

## [04. Responses](04.responses)
## [05. Filters](05.filters)
## [06. Account details](06.account_details)
## [07. Forwarding](07.forwarding)