---
title: Courriel Web Disroot
published: true
visible: true
indexed: true
updated:
        last_modified: "Avril 2020"
        app: Roundcube Webmail
        app_version: 1.4.2
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Courriel Web Disroot

Un webmail, ou courrier électronique basé sur le Web, est un service de courrier électronique auquel on peut accéder à l'aide d'un navigateur Web. Cela signifie que vous n'avez pas besoin d'installer un logiciel client de messagerie sur votre appareil. Le principal avantage du webmail par rapport à l'utilisation d'un client de messagerie de bureau est la possibilité d'envoyer et de recevoir des e-mails n'importe où à partir d'un navigateur Web.

La solution de webmail **Disroot** est optimisée par **Roundcube**.


![](logo.png)

# Qu'est-ce que Roundcube ?
Roundcube est un logiciel de webmail gratuit et open source qui fournit toutes les fonctionnalités que vous attendez d'un client de messagerie : support complet pour les messages MIME et HTML, identités d'expéditeurs multiples, carnet d'adresses avec groupes et connecteurs LDAP, liste de messages en fil de discussion, vérification orthographique, support pour les listes de contrôle d'accès (ACL), fonctions d'import/export et support pour le cryptage PGP parmi beaucoup d'autres fonctionnalités.

Grâce au tutoriel suivant, nous allons apprendre à utiliser **Roundcube** et à connaître ses fonctionnalités.

----

# Table des matières

## [01. Mise en route](Roundcube/01.mise en route)
  - Vue d'ensemble
  - Tâches et opérations de base

## [02. Paramètres](settings)
  - [01. Préférences](settings/preferences)
    - Interface utilisateur
    - Vue des boîtes aux lettres
    - Affichage des messages
    - Composer des messages
    - Contacts
    - Dossiers spéciaux
    - Paramètres du serveur
    - Suppression des anciens messages
    - Points forts du message
  - [02. Dossiers](settings/folders)
  - [03. identités](settings/identities)
    - Identité par défaut
    - Ajouter d'autres identités / alias
    - Envoyer un email avec une autre identité
  - [04. réponses](settings/responses)
  - [05. filtres](settings/filters)
  - [06. Détails du compte](settings/account_details)
  - [07. Renvoi](settings/forwarding)

## [03. Courriel](email)
  - Composer un e-mail

## [04. Contacts](contacts)
  - Listes de contacts
  - Carnet d'adresses
  - Groupes
