---
title: Disroot Webmail
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: true
---

# Disroot Webmail
Una webmail, o e-mail basata sul web, è un servizio di posta elettronica a cui è possibile accedere tramite il browser web. Ciò significa che non è necessario installare un software di posta elettronica sul dispositivo. Il principale vantaggio della webmail rispetto all'uso di un client di posta elettronica desktop è la possibilità di inviare e ricevere e-mail ovunque da un browser web. 
La webmail di **Disroot** si basa sul software **Roundcube**.


![](roundcube/rc_logo.png)

# Cosa è Roundcube?
Roundcube è un software webmail gratuito e open source che fornisce tutte le funzionalità che ti aspetteresti da un client di posta elettronica: supporto completo per messaggi MIME e HTML, identità di più mittenti, rubrica con gruppi e connettori LDAP, elenco di messaggi thread, controllo ortografico, supporto per elenchi di controllo di accesso (ACL), funzioni di importazione/esportazione e supporto per la crittografia PGP tra molte altre funzionalità.
**[[Sito web di Roundcube](https://roundcube.net/)] | [[Codice sorgente](https://github.com/roundcube/roundcubemail/)]**

Attraverso il seguente tutorial impareremo ad utilizzare **Roundcube** e ne conosceremo le caratteristiche.

----

# Indice

# [Guida migrazione](migration)
  - [FAQ sulla migrazione](migration/faq)
  - [Backup/esportazione e importazione dei contatti](migration/backup)

# [Roundcube](roundcube)
## [01. Iniziamo](roundcube/getting_started)
  - Panoramica
  - Task e impostazioni principali

## [02. Impostazioni](roundcube/settings)
  - [01. Preferenze](roundcube/settings/preferences)
    - Interfaccia grafica
    - Visualizzazione della casella di posta
    - Visualizzazione messaggi
    - Composizione messaggi
    - Contatti
    - Cartelle speciali
    - Impostazioni del server
    - Eliminare vecchi messaggi
    - Messaggi in evidenza
  - [02. Cartelle](roundcube/settings/folders)
  - [03. Identità](roundcube/settings/identities)
    - Identità di default
    - Aggiungere una nuova identità/alias
    - Inviare un messaggio da un alias
  - [04. Risposte](roundcube/settings/responses)
  - [05. Filtri](roundcube/settings/filters)
  - [06. Dettagli account](roundcube/settings/account_details)
  - [07. Forwarding](settings/forwarding)

## [03. Email](roundcube/email)
  - Comporre un messaggio

## [04. Contatti](roundcube/contacts)
  - Lista contatti
  - Importare contatti
  - Rubrica
  - Gruppi
