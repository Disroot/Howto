---
title: Disroot Webmail
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: true
---

# Disroot Webmail

A webmail, or web-based email, is an email service that can be accessed using the web browser. This means we don't need to install an email client software on our devices. The main advantage of webmail over the use of a desktop email client is the ability to send and receive email anywhere from a web browser.

**Disroot** webmail solution is powered by **Roundcube**, a free and open source webmail software.

Through the following tutorial, we will learn how to use **Roundcube** and get to know its features.

# Webmail basics
## [01. Getting started](getting_started)
  - Overview
  - Tasks and basic operations

## [02. Settings](settings)
  - [01. Preferences](settings/preferences)
    - User interface
    - Mailbox view
    - Displaying messages
    - Composing messages
    - Contacts
    - Special folders
    - Server settings
    - Deleting old messages
    - Message highlights
  - [02. Folders](settings/folders)
  - [03. Identities](settings/identities)
    - Default identity
    - Add other identities / aliases
    - Sending an email with another identity
  - [04. Responses](settings/responses)
  - [05. Filters](settings/filters)
  - [06. Account details](settings/account_details)
  - [07. Forwarding](settings/forwarding)

## [03. Email](email)
  - Composing an email

## [04. Contacts](contacts)
  - Contacts lists
  - Importing contacts
  - Address book
  - Groups

## [05. Encryption](encryption)