---
title: D·Scribe
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - lemmy
        - dscribe
page-toc:
    active: false
---

![](scribe.svg)

# D·Scribe: Disroot's Lemmy instance

**Lemmy** is a free and open source software for federated social link aggregation and discussion forum. This means that it allows us to aggregate digital content such as online news, blog posts, images and podcasts in one place and organized by topics within different communities with common interests. Users can post text, links, images or videos and discuss it with others. It also has a voting system that helps to bring the most interesting items to the top.

As it is a self-hosted software, there are many "instances", some on specific topics, others more general, but all with the ability to federate with each other thanks to the **ActivityPub** protocol, that is, they can communicate with each other in a similar way as e-mail does. Moreover, it can communicate with any other software using this same protocol, such as **Mastodon**, **Pleroma**, **PixelFed**, **PeerTube**, **Misskey**, **Funkwhale** or **Plume**.

Let's see how it works and its different options to start spreading news, ideas, content and more, and generate open spaces for debate, free from the limitations imposed by commercial and centralized corporate software.

---

# Table of Content
1. [Getting started](01.gettingstarted)
2. [Interacting](02.interacting)
3. [Communities creation & Moderation](03.communities)