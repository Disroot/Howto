---
title: D·Scribe
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - lemmy
        - dscribe
page-toc:
    active: false
---

![](scribe.svg)

# D-Scribe : L'instance Lemmy de Disroot

**Lemmy** est un logiciel libre et open source d'agrégation de liens sociaux fédérés et de forum de discussion. Cela signifie qu'il nous permet d'agréger des contenus numériques tels que des nouvelles en ligne, des articles de blog, des images et des podcasts en un seul endroit et organisés par thèmes au sein de différentes communautés ayant des intérêts communs. Les utilisateurs peuvent publier des textes, des liens, des images ou des vidéos et en discuter avec d'autres. Il dispose également d'un système de vote qui permet de faire apparaître les éléments les plus intéressants en tête de liste.

Comme il s'agit d'un logiciel auto-hébergé, il existe de nombreuses "instances", certaines traitant de sujets spécifiques, d'autres plus générales, mais toutes ayant la capacité de se fédérer grâce au protocole **ActivityPub**, c'est-à-dire qu'elles peuvent communiquer les unes avec les autres de la même manière que le courrier électronique. De plus, il peut communiquer avec n'importe quel autre logiciel utilisant ce même protocole, comme **Mastodon**, **Pleroma**, **PixelFed**, **PeerTube**, **Misskey**, **Funkwhale** ou **Plume**.

Voyons comment il fonctionne et quelles sont ses différentes options pour commencer à diffuser des nouvelles, des idées, du contenu et plus encore, et générer des espaces de débat ouverts, libres des limitations imposées par les logiciels commerciaux et centralisés des entreprises.

---

# Table des matières

1. [Pour commencer](01.gettingstarted)
2. [Interagir](02.interacting)
3. [Création et modération de communautés](03.communities)