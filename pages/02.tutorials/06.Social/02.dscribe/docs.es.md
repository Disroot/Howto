---
title: D·Scribe
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - lemmy
        - dscribe
page-toc:
    active: false
---

![](scribe.svg)

# D·Scribe: la instancia de Lemmy de Disroot

**Lemmy** es un software libre y de código abierto para el agregado federado de links sociales y foros de discusión. Esto quiere decir que nos permite agrupar contenido digital como noticias en línea, publicaciones de blogs, imágenes y podcasts en un solo lugar y organizados por tópicos o temáticas dentro de diferentes comunidades con intereses comúnes. Las personas usuarias pueden publicar texto, vínculos, imágenes o videos y discutirlos con otras. También cuenta con un sistema de votación que ayuda a destacar los artículos más interesantes.

Como se trata de un software autoalojado, hay muchas "instancias", algunas sobre temas específicos, otras más generales, pero todas con la capacidad de federar entre sí gracias al protocolo **ActivityPub**, esto es, pueden comunicarse unas con otras de manera similar a como lo hace el correo electrónico. Es más, puede comunicarse con otros programas que utilizan este mismo protocolo, como **Mastodon**, **Pleroma**, **PixelFed**, **PeerTube**, **Misskey**, **Funkwhale** o **Plume**.

Veamos cómo funciona y sus diferentes opciones para comenzar a difundir noticias, ideas, contenido y más, y generar espacios abiertos de debate, libres de las limitaciones que imponen el software comercial y centralizado de las corporaciones.

---

# Índice
1. [Primeros pasos](01.gettingstarted)
2. [Interactuando](02.interacting)
3. [Creación de comunidades & Moderación](03.communities)
