---
title: Fediverse
subtitle: Akkoma
icon: fa-asterisk
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - fediverse
        - akkoma
        - fe
page-toc:
    active: false
---

![](fediverse.png)

# Qu'est-ce que le Fediverse ?

Le **Fediverse** (des mots "fédération" et "univers") est un réseau de serveurs indépendants qui fournissent différents types de logiciels et de services fédérés, c'est-à-dire qui peuvent interagir et communiquer entre eux sur la base de normes de communication et de fonctionnement ouvertes. Il se compose de réseaux sociaux, de services de blogging, de microblogging et d'hébergement de fichiers dédiés principalement à la création et à la publication de contenus. Ses principales caractéristiques sont sa nature décentralisée, son interopérabilité, la prédominance de protocoles standard ouverts et de logiciels sous licence libre et l'absence d'une autorité ou d'une entité centrale qui le concentre et le contrôle. Contrairement aux services privés centralisés tels que **Facebook**, **Twitter** ou **Instagram**, ce réseau permet aux utilisateurs non seulement de communiquer et de collaborer entre eux quel que soit le logiciel ou le serveur qu'ils choisissent, mais aussi d'avoir le contrôle de leurs données.

Si vous avez lu notre [**Déclaration de mission**] (https://disroot.org/mission-statement) (si ce n'est pas le cas, nous vous invitons à le faire), vous comprendrez à quel point ce réseau est important pour nous sur le plan conceptuel et culturel.

Au sein du Fediverse, il existe un nombre croissant de plateformes et de services axés sur les utilisateurs, leur autonomie et leur vie privée. Nous les avons utilisés et promus depuis le tout début de **Disroot** et avons essayé différentes approches. Cette fois, nous avons décidé de faire des pas plus petits mais plus fermes dans cette direction.

Actuellement, le protocole [**ActivityPub**] (https://www.w3.org/TR/activitypub/) est le principal moteur de ce Fediverse. Son adoption rapide et généralisée est due à son développement constant et à la maturité qu'il a atteinte. Pour s'en convaincre, il suffit de regarder le large éventail de plateformes et de services qui le mettent en œuvre : **Mastodon**, **Pleroma**, **Nextcloud**, **PeerTube**, **Funkwhale**, **Friendica**, **PixelFed**, **BookWyrm**, **Lemmy**, **Plume**, **Writefreely**, **Misskey**, entre autres.

Pour tout cela, et parce que "nous voulons encourager l'émergence d'un plus grand nombre de fournisseurs de services indépendants et décentralisés qui travaillent en coopération - et non en concurrence - les uns avec les autres", nous avons décidé de créer **FEDisroot**, l'instance de notre **Akkoma**. La première des nombreuses graines que nous espérons planter dans ce merveilleux écosystème.

Avec ce nouveau projet, nous espérons non seulement vous fournir un outil de communication accessible, ouvert et utile, mais aussi collaborer à la construction d'un "réseau décentralisé vraiment fort" qui nous aidera à "déraciner la société du statu quo actuel régi par la publicité, le consumérisme et le pouvoir du plus offrant".

Donc...

**Vous êtes tous les bienvenus dans le Fediverse et dans FEDisroot**.

---
