---
title: Fediverse
subtitle: "FEDisroot, D·Scribe"
icon: fa-asterisk
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - fediverse
        - akkoma
        - lemmy
        - fedisroot
        - dscribe
page-toc:
    active: false
---

![](fediverse.png)

# Cosa è il Fediverso?

Il **Fediverso** (dalle parole "federazione" e "universo") è una rete di server indipendenti che forniscono diversi tipi di software e servizi federati, cioè in grado di interagire e comunicare tra loro sulla base di standard di comunicazione e funzionamento aperti. Si tratta di reti sociali, servizi di blogging, microblogging e file hosting dedicati principalmente alla creazione e alla pubblicazione di contenuti. Le sue caratteristiche principali sono la natura decentralizzata, l'interoperabilità, la predominanza di protocolli standard aperti e di software a licenza libera e l'assenza di un'autorità o di un'entità centrale che lo concentri e lo controlli. A differenza di servizi privati centralizzati come **Facebook**, **Twitter** o **Instagram**, questa rete permette agli utenti non solo di comunicare e collaborare tra loro indipendentemente dal software o dal server scelto, ma anche di avere il controllo sui propri dati.

Se avete letto la nostra [**mission**](https://disroot.org/en/mission-statement) (se non l'avete fatto, vi invitiamo a farlo) allora capirete quanto sia importante per noi dal punto di vista concettuale e culturale.

All'interno del Fediverso, c'è un numero crescente di piattaforme e servizi incentrati sugli utenti, sulla loro autonomia e sulla privacy. Li abbiamo utilizzati e promossi fin dall'inizio di **Disroot** e abbiamo provato diversi approcci. Questa volta abbiamo deciso di fare passi più piccoli ma più decisi in questa direzione.


Attualmente, il protocollo [**ActivityPub**](https://www.w3.org/TR/activitypub/) è il motore principale del Fediverso. La sua rapida e diffusa adozione è dovuta al costante sviluppo e alla maturità raggiunta. A riprova di ciò, basta guardare l'ampia gamma di piattaforme e servizi che lo implementano: **Mastodon**, **Pleroma**, **Nextcloud**, **PeerTube**, **Funkwhale**, **Friendica**, **PixelFed**, **BookWyrm**, **Lemmy**, **Plume**, **Writefreely**, **Misskey** ed altri ancora..

Per tutto questo, e perché _"vogliamo incoraggiare l'emergere di più fornitori di servizi indipendenti e decentralizzati che lavorino in cooperazione - non in competizione - tra loro"_ abbiamo deciso di creare **FEDisroot** e **D-Scribe**, rispettivamente le nostre istanze **Akkoma** e **Lemmy**. Il primo dei molti semi che speriamo di piantare in questo meraviglioso ecosistema.

Con questi nuovi progetti, speriamo non solo di fornirvi uno strumento di comunicazione accessibile, aperto e utile, ma anche di collaborare alla costruzione di "una rete decentralizzata veramente forte" che ci aiuterà a "sradicare la società dall'attuale status quo governato dalla pubblicità, dal consumismo e dal potere del miglior offerente".

quindi...

**siete tutti i benvenuti nel Fediverso**.

---
