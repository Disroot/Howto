---
title: Client de chat: Pidgin
updated:
    app: Pidgin
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - chat
        - pidgin
        - xmpp
page-toc:
    active: true
---

![](/home/icons/pidgin.png)

Pidgin est une application de messagerie multiplateforme (fonctionne sur la plupart des systèmes d'exploitation) et multiprotocole. Elle prend en charge un grand nombre de protocoles de messagerie différents tels que: xmpp, skype, AIM, TOX, Whatsapp, facebook, etc, de sorte que vous pouvez avoir toute votre messagerie faite avec une seule application.

# Installer Pidgin.
Si vous utilisez **MS Windows** / **MacOS**, vérifiez le [site web de pidgin](http://pidgin.im/download/) et téléchargez une version.<br>
Si vous êtes un utilisateur **GNU/Linux**, vérifiez votre gestionnaire de paquets et télécharger pidgin. (*vérifiez pour des plugins supplémentaires pendant que vous y êtes.*)

# Configurer Pidgin
Une fois installé, la première fois que vous lancez **Pidgin**, vous serez accueilli par un message de bienvenue comme celui-ci:

![](en/pidgin1.png)

Pour continuer, il faut **ajouter un compte**.
Si ce n'est pas la première fois que vous exécutez pidgin vous devriez utiliser le menu:

![](en/pidgin2.png)

# Mise en place du compte.
Tout d'abord, choisissez le protocole xmpp dans la liste déroulante:

![](en/pidgin3.png)

Remplissez ensuite les informations requises:

![](en/pidgin4.png)

!Note! Si vous souhaitez utiliser un avatar (facultatif), vous pouvez sélectionner "Utiliser cette icône de compagnon pour ce compte" et parcourir votre système de fichiers local (ordinateur) pour l'image que vous voulez utiliser.

4. Appuyez sur "**Ajouter**" et c'est fini!
Maintenant, vous êtes prêt à convertir les gens de quitter les applications malveillantes des entreprises et de sauter sur le *vaisseau* de la fédération.

![](en/pidgin5.png)

**Tous vos contacts sont synchronisés entre tous les clients, de sorte que vous pouvez utiliser le chat Disroot sur tous les périphériques en même temps.**
