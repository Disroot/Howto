---
title: Desktop Clients
updated:
published: true
visible: true
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - chat
        - xmpp
page-toc:
    active: false
---

# Chat Clients for Desktop

### Multiplatform clients
- [Pidgin](pidgin)
- [Gajim](gajim)

### GNU/Linux clients
- [Dino](dino)
