---
title: Git
subtitle: Forgejo
icon: fa-code-fork
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - git
        - forgejo
page-toc:
    active: false
---

![](git.png)

**Disroot's Git** is a tool that allows us to manage changes in our projects in an efficient and collaborative way and it is powered by **Forgejo**.

**Forgejo** is a source code repository management software based on Git. It provides a web interface for managing Git repositories, access control, issue tracking and other features related to collaborative software development.

Below we will find guides and tutorials related to **Forgejo** and **Git**.

---

