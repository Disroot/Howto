---
title: Appels
subtitle: Bases, paramètres, clients.
icon:  fa-microphone
published: true
taxonomy:
    category:
        - docs
        - topic
page-toc:
    active: false
---

# Appels Disroot

![](calls.logo.png)

Le service **Appel de Disroot** est un logiciel de vidéoconférence libre et gratuit, basé sur [**Jitsi**](https://jitsi.org/). Il permet de réaliser des conférences vidéo et audio de haute qualité, avec autant de partenaires que l'on souhaite. Il permet également de diffuser son bureau ou seulement certaines fenêtres aux autres participants à l'appel.

Il peut être utilisé sur des téléphones portables, des tablettes, des ordinateurs de bureau ou via le web et ne nécessite pas de créer un compte.
