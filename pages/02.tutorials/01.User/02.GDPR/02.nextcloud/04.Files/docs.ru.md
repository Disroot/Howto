---
title: 'Nextcloud: Экспорт файлов и заметок'
published: true
indexed: true
updated:
    last_modified: "June 2020"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - files
        - notes
        - gdpr
visible: true
page-toc:
    active: false
---

Вы можете скачать свои файлы так же просто, как и в случае любого приложения **Nextcloud**.

  - Войдите в [облако](https://cloud.disroot.org)
  - Выберите приложение **Файлы**
  - Выберите все файлы, установив флажок
  - Затем нажмите меню **Действия** и выберите *Скачать*

![](ru/export_files.gif)
