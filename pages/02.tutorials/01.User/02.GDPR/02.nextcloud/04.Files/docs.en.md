---
title: 'Export data: Nextcloud Files & Notes'
published: true
indexed: false
updated:
    last_modified: "June 2020"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - files
        - notes
        - gdpr
visible: true
page-toc:
    active: false
---

You can download your files as easy as in case of any **Nextcloud** app.

  - Login to your [cloud](https://cloud.disroot.org)
  - Select **Files** app
  - Select all files by clicking on the checkbox
  - Then click on the **Actions** menu and select *Download*

![](en/export_files.gif)
