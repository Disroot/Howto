---
title: 'Nextcloud: Экспорт закладок'
published: true
indexed: true
updated:
    last_modified: "June 2020"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
        - bookmarks
        - gdpr
visible: true
page-toc:
    active: false
---

Экспорт ваших закладок, хранящихся в облаке **Disroot**, очень прост.

  - Войдите в [облако](https://cloud.disroot.org).
  - Выберите приложение **Закладки**.

  ![](ru/select.gif)

  - Выберите **Параметры** (в нижней части левой боковой панели) и нажмите кнопку **Экспортировать**.

  ![](ru/export.gif)

  - Выберите, где сохранить файл.
