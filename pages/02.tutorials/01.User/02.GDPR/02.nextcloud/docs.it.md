---
title: GDPR: Nextcloud
published: true
indexed: false
taxonomy:
    category:
        - docs
    tags:
        - user
        - gdpr
visible: true
page-toc:
    active: false
---
## Come esportare i tuoi...

  - [**Documenti e note**](files)
  - [**Contatti**](contacts)
  - [**Calendari**](calendar)
  - [**Segnalibri**](bookmarks)
