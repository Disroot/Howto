---
title: 'Email: Export'
published: true
indexed: true
updated:
    last_modified: "Novembre 2020"		
    app: RainLoop
    app_version:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - email
        - rgpd
visible: true
page-toc:
    active: false
---

# Exporter vos emails

Pour exporter tous vos emails de **Disroot Webmail**, nous vous suggérons d'utiliser un client de messagerie, comme **Thunderbird**.

## RainLoop
**RainLoop** le logiciel que nous utilisons actuellement, ne permet pas d'effectuer cette tâche de manière pratique : les emails doivent être exportés un par un.

* Connectez-vous à **Disroot Webmail**.
* Sélectionnez le mail que vous voulez exporter.
* A droite, cliquez sur l'icône de réponse.
* Une liste d'options s'ouvre, sélectionnez la dernière : **Télécharger comme fichier .eml**.
* Enfin, il vous sera demandé ce que vous souhaitez faire avec le fichier, enregistrez-le sur votre appareil.

![](en/rl_export.gif)

! ! ##### **NOTICE** : Vous devez répéter cette procédure pour chaque e-mail que vous souhaitez exporter.

----

## Exportation d'emails depuis Thunderbird

**Thunderbird** n'est pas livré avec des outils pour exporter et importer du courrier de façon native. Pour cela, il est nécessaire d'installer un module complémentaire.

L'installation du module complémentaire et le processus d'exportation des courriels sont assez simples.

### Installation du module complémentaire ImportExportTools

* Lancez Thunderbird
* Allez dans **Tools** dans la barre de menu et sélectionnez **Add-ons**.
* Dans le champ de recherche des modules complémentaires, tapez _export tools_, puis appuyez sur la touche Entrée.
* Trouvez **ImportExportTools NG**, un petit logiciel **GPL** qui ajoute des outils d'import/export.
* Cliquez sur **Ajouter à Thunderbird**.
* Après avoir installé et donné les autorisations nécessaires pour l'add-on, vous êtes maintenant prêt à sauvegarder tous vos emails.

![](en/export.mp4?resize=1024,576&loop)

Cet add-on ajoute un nouvel élément de menu dans les **Outils** et les menus contextuels.

### Exportation de vos emails
Dans **Thunderbird**, sélectionnez un dossier ou un répertoire que vous voulez sauvegarder et faites un clic droit.
Le sous-menu de l'outil d'importation/exportation affiche un certain nombre d'actions que vous pouvez effectuer pour importer ou exporter divers types d'informations dans et hors de **Thunderbird**.

Vous pouvez exporter :
  * Des dossiers individuels et/ou tous les messages du dossier (EML, HTML, PDF, CSV ou texte en clair)
  * Exporter des fichiers individuels ou un seul fichier
  * Exporter les index en texte brut ou CSV.
  * Exportation des messages depuis le dialogue de recherche
  * Exportation de profils complets ou seulement des fichiers d'emails

Et import :
  * Fichiers de boîtes aux lettres (fichiers Mbox incluant les structures)
  * Profils
  * Fichiers EML et EMLX
  * Fichiers individuels ou tous les répertoires
