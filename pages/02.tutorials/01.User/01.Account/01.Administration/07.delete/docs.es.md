---
title: 'Dar de baja la cuenta'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - cuenta
        - eliminar
page-toc:
    active:
---

# Suprimir mi cuenta
Si por alguna razón quieres cancelar tu cuenta, solo haz clic en esta opción.

![](es/tablero_borrar.png)

Si estás segurx respecto de la eliminación, marca la casilla **Acepto** y finalmente **Suprimir**.

![](es/borrar.png)

!! #### AVISO<br>
!! **Este proceso es irreversible. Una vez confirmado, no podrás iniciar sesión en tu cuenta o solicitar que sea restaurada más adelante.**<br>
!! **Toda la información será eliminada dentro de las 48 horas luego de confirmar la cancelación y tu usuarix actual no estará disponible cuando crees una cuenta nueva.**
