---
title: 'Account Deletion'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - deletion
page-toc:
    active:
---

# Elimina il tuo account
Se per qualsiasi motivo desideri eliminare il tuo account, fai clic su questa opzione. 

![](en/dashboard_delete.png)

Una volta che sei sicuro di eliminare l'account, seleziona la casella **Accetto** e infine **Elimina**. 

![](en/delete.png)

!! #### ATTENZIONE<br>
!! **Questo processo è irreversibile. Una volta confermato, non potrai accedere al tuo account o chiederne il ripristino in un secondo momento.**<br>
!! **Tutti i tuoi dati rimanenti verranno eliminati entro 48 ore e il tuo nome utente attuale non sarà disponibile durante la creazione di un nuovo account.** 