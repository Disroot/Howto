---
title: 'Удаление учётной записи'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - deletion
page-toc:
    active:
---

# Удалить свою учётную запись
Если по какой-либо причине вы хотите удалить свою учетную запись, просто нажмите на эту опцию.

![](ru/dashboard_delete.png)

Если вы уверены, что хотите удалить учетную запись, установите флажок **Принимаю** и нажмите **Удалить**.

![](ru/delete.png)

!! #### ВНИМАНИЕ<br>
!! **Этот процесс необратим. После подтверждения вы не сможете войти в свою учетную запись или запросить ее восстановление позже.**<br>
!! **Вся информация будет удалена в течение 48 часов после подтверждения, а ваше текущее имя пользователя не будет доступно при создании новой учетной записи.**
