---
title: 'Управление учётной записью Disroot'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: true
---

# [Пользовательский центр самообслуживания](ussc)
- Обзор
- [Сброс пароля](ussc/pwd_reset)
- [Регистрация новой учётной записи](ussc/new_reg)

## [Изменить пароль](password)
Обновить ключ шифрования **облачного хранилища**

## [Настроить контрольные вопросы](questions)

## [Обновить профиль](profile)

## [Формы запросов](forms)

## [Информация об учётной записи](info)

## [Удалить учётную запись](delete)
