---
title: 'Preguntas de seguridad'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - cuenta
        - gestión
page-toc:
    active:
---

# Configurar las preguntas de seguridad

![](es/tablero_preguntas.png)

En caso que olvides/pierdas tu contraseña, puedes restablecerla sin intervención de lxs administradorxs configurando primero las preguntas de seguridad. Para hacerlo, haz clic en esta opción.

El proceso es bastante sencillo.

- Selecciona ***Configurar preguntas de seguridad***.

 ![](es/preg_01.png)

- Escribe la primera pregunta y su respuesta. Luego selecciona las siguientes dos preguntas a partir de las opciones de la lista desplegable y escribe también las respuestas.

  ![](es/preg_02.png)

- Una vez que las respuestas cumplan con los requisitos, solo haz clic en **Guardar respuestas** y finalmente en **Continuar**

  ![](es/preg_03.png)
