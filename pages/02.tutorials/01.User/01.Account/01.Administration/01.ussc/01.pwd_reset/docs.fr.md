---
title: 'Réinitialisation du mot de passe'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - compte
        - mot de passe
page-toc:
    active: false
---

# Réinitialisation de votre mot de passe
Pour réinitialiser votre mot de passe, vous devez avoir préalablement

- a. configuré les [**questions de sécurité**](../../questions) et que, bien sûr, vous vous souveniez des réponses ;
- b. entré un [**e-mail secondaire**](../../profil) (pendant l'enregistrement ou avant ce processus), sinon le code sera envoyé à l'adresse du même compte que vous essayez de réinitialiser.

!! ##### Si aucune des conditions ci-dessus ne s'applique à votre cas, il ne sera pas possible de récupérer le mot de passe et le compte pourra être considéré comme perdu.

![](en/reset.gif)

1. Allez sur [https://user.disroot.org](https://user.disroot.org)
2. Cliquez sur le bouton "**Mot de passe oublié**".
3. Tapez votre nom d'utilisateur et ensuite **Recherche**.
4. Choisissez l'une des méthodes suivantes
- **Questions et réponses secrètes** : Si vous choisissez cette option, vous serez invité à répondre aux questions.<br> *Veuillez noter que les réponses sont sensibles à la casse*.

- **Vérification par SMS/Email** : Cette option vous permet de demander la réinitialisation du mot de passe via un courriel secondaire.

!! ##### Souvenez-vous :
!! ##### Le compte et sa sécurité sont sous votre responsabilité, **nous ne pouvons pas réinitialiser votre mot de passe**, alors veuillez prendre toutes les mesures nécessaires pour le garder en sécurité.
