---
title: 'Manage your Disroot Account'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: true
---

# [User Self-service Center](ussc)
- Overview
- [Password reset](ussc/pwd_reset)
- [New account registration](ussc/new_reg)

## [Change your password](password)
Update your **Cloud** encryption key

## [Setup your Security Questions](questions)

## [Update your Profile](profile)

## [Request forms](forms)

## [Account information](info)

## [Delete your Account](delete)
