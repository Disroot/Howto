---
title: DisApp
published: true
visible: true
indexed: true
updated:
    last_modified: "Diciembre, 2020"		
    app: DisApp
    app_version: 2.0.2
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - disapp
page-toc:
    active: false
---

# DisApp:<br> La aplicación de la Comunidad de Disroot

![](thumb.png)

## **Una App para controlarlas a todas**

---
## Contexto
El disrooter **Massimiliano** vio el potencial de una app de **Disroot** y decidió aceptar el desafío con un abordaje inesperado. Desarrolló la **"navaja suiza" de Disroot**, una aplicación que ayuda y guía a lxs disrooters recomendando aplicaciones, consejos y tutoriales sobre cómo configurar todo.

## ¿Qué hace?
La app seleccionará el mejor programa (en nuestra opinión) para el correo, chat, etc, y para aquellos servicios que no tengan uno específico los abrirá en el navegar integrado. La aplicación también proporciona direcciones a todos los tutoriales de la comunidad, que hemos ido reuniendo a lo largo de los años, para ayudar a la gente a utilizar los servicios que proveemos.

## ¿Dónde puedo conseguirla?
La aplicación está disponible desde la tienda de apps **F-Droid**, [**aquí**](https://f-droid.org/es/packages/org.disroot.disrootapp/).
