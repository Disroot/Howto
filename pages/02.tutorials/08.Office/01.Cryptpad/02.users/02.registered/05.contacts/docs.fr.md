---
title: 'Contacts'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - contacts
        - cryptpad
page-toc:
    active: true
---

# Contacts
L'utilisation des contacts CryptPad rend la collaboration plus sûre et plus simple.

## Ajouter un contact
Il y a deux façons d'ajouter un contact :

1. **En partageant votre profil**
  - Allez dans le menu utilisateur -> **Profil** et cliquez ensuite sur le bouton **PARTAGER**.
  - Une fois copié, vous pouvez l'envoyer par le moyen de communication de votre choix (sécurisé de préférence) en collant l'adresse.

![](en/add_contact_share.png)

2. **En envoyant une demande de contact**
 - Allez sur la page de profil de l'utilisateur que vous voulez ajouter à vos contacts et cliquez sur le bouton **Envoyer une demande de contact**.

![](en/contact_request.png)

## Gérer les contacts
Pour accéder à vos contacts, allez dans le menu utilisateur -> **Contacts**. Ils sont tous listés à gauche.

![](en/contacts_options.gif)

Pour chaque contact, vous pouvez :
  - **Mettre en sourdine** leurs messages et notifications.
  - **Les supprimer**.
  - Vérifier s'ils sont en ligne.

## Chat avec les contacts
Pour chatter avec une personne de votre liste de contacts, cliquez simplement sur son nom d'utilisateur et le chat s'ouvrira dans la fenêtre principale.

![](en/chat.gif)

Vous pouvez également **charger** ou **supprimer** l'historique du chat (s'il y en a un) avec les options situées à droite de la fenêtre principale du chat.
