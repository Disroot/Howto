---
title: 'Registered user'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - profile
        - configurations
        - cryptpad
page-toc:
    active: true
---

# Registered users
Registered users have additional functionalities enabled:

- **Storage space** for documents is personal, permanent and can host multimedia files.
- **File management options**:
  * add a password, an expiry date, or an access list.
  * organize documents in folders, shared folders, or with tags and templates.
- Create and work with **Teams**.
- **Profile** customization
- **Contacts** features (to share documents and chat with them)
- **Notifications** for interactions between contacts.

---
In the next chapters we will learn how to configure CryptPad, its basics and how to manage CryptDrive

- ## [Configurations](configurations)
- ## [CryptDrive](cryptdrive)
- ## [Share & Access](sharing)
