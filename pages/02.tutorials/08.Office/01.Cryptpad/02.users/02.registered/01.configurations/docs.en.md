---
title: 'Configurations'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - profile
        - configurations
        - cryptpad
page-toc:
    active: true
---

# Configurations
To access your configurations, click on the user menu at the top right (the first time you log in it will show the first 2 letters of your display name).

![](en/settings.png)


In the user menu you will find the following options:

- [Profile](../profile)
- [Teams](../teams)
- [Calendar](../calendar)
- [Contacts](../contacts)
- [Settings](../settings)
