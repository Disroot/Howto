---
title: 'Comptes'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilsateur
        - compte
        - cryptpad
page-toc:
    active: true
---

# Types de comptes

**Le CryptPad de Disroot** propose deux types de comptes, dont aucun ne nécessite de fournir des informations personnelles :

1. **Invité** : vous pouvez accéder et utiliser toutes les applications et les fonctions communes de collaboration et de partage sans vous enregistrer. Ce type de compte a le **temps de stockage limité à 90 jours d'inactivité** (par document), les fonctionnalités du **CryptDrive** réduites (vous pouvez stocker les blocs visités pour les ouvrir plus tard) et vous ne pouvez pas modifier et personnaliser votre profil.

2. **Enregistré** : avec ce type de compte, vous accédez non seulement aux fonctions d'invité (avec des fonctions supplémentaires) mais aussi à l'ensemble des fonctionnalités sociales, de collaboration et de **CryptDrive** et il n'a pas de limitation de temps de stockage (nous verrons ces fonctionnalités en détail plus tard).

!! **NOTE:**
!! Vos informations d'identification **Disroot** ne fonctionneront pas dans notre **CryptPad**, vous devez donc créer un compte séparé.


# Enregistrement d'un compte
Pour enregistrer un nouveau compte, cliquez sur le bouton **S'inscrire** en haut à droite.

![](en/signup_01.png)

Remplissez le formulaire d'inscription :

![](en/signup_02.png)

**Nom d'utilisateur** : le nom que vous utiliserez pour vous connecter à **CryptPad**. Il est différent du **Nom d'affichage** (qui est visible par les autres utilisateurs) et ne peut être modifié une fois le compte créé.

**Mot de passe** : il est fortement recommandé d'utiliser un mot de passe fort et de le sauvegarder en toute sécurité. Rappelez-vous qu'en raison de la nature privée de **CryptPad**, il est impossible pour les administrateurs d'accéder ou de récupérer vos données en cas d'oubli/de perte de votre mot de passe et/ou de votre nom d'utilisateur.

![](en/signup_03.png)

Lisez et acceptez les conditions du service et enfin cliquez sur **S'inscrire**. C'est tout.

![](en/signup.mp4?resize=1024,576&autoplay=0&loop)

!! **Si vous avez des documents créés en tant qu'utilisateur invité (non enregistré), vous pouvez les importer dans votre compte**.
