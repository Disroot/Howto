---
title: Whiteboard
published: true
visible: false
indexed: true
updated:
        last_modified: "September 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - tableau blanc
        - applications
visible: true
page-toc:
    active: false
---

# Tableau blanc

Dessinez, déplacez des éléments, insérez des images et bien plus encore sur un tableau blanc que vous pouvez exporter sous forme de fichier image.
