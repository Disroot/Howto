---
title: Feuille
published: true
visible: false
indexed: true
updated:
        last_modified: "Septembre 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - feuille de calcul
        - feuille
        - applications
visible: true
page-toc:
    active: false
---

# Feuille

Créez et modifiez des feuilles de calcul.
