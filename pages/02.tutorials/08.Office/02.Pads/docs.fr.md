---
title: 'Pads'
updated:
published: true
visible: true
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - Pad
        - Etherpad
page-toc:
  active: false
---

![](/home/icons/etherpad.png)
**Etherpad** est une application collaborative d'édition de documents multi-utilisateurs en temps réel. Vous pouvez y accéder directement depuis votre navigateur à : [https://pad.disroot.org](https://pad.disroot.org).<br>Aucun compte utilisateur n'est nécessaire pour l'utiliser. Cependant, notre cloud est livré avec un plugin très pratique qui vous aide à garder une trace de tous vos pads, comme s'il s'agissait de l'un de vos fichiers.

!! **Les "Pads" ne sont pas des fichiers contenant vos données mais des liens vers votre travail stocké sur https://pad.disroot.org**

# L'idée des pads...
... est très simple. C'est un éditeur de texte qui vit sur le Web. Tout ce que vous tapez est écrit automatiquement sur votre pad. Vous pouvez travailler sur un document avec plusieurs personnes en même temps sans avoir besoin d'enregistrer et de transmettre des copies des documents les unes aux autres. Une fois votre travail terminé, vous pouvez exporter le pad dans un format de votre choix.

### [Etherpad](etherpad)
- Éditeur collaboratif de documents texte web

### [Padland](padland)
- Outil de gestion des pads pour Android
