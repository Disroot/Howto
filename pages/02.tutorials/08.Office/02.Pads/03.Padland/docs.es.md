---
title: Pads: Padland
published: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

|![](/start/icons/padland.png)|
|:--:|
|Padland es una herramienta para gestionar, compartir, recordar y leer documentos colaborativos basada en la tecnología Etherpad para Android.|
