---
title: "Trabajando con Editores"
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribuir
        - git
        - editores
        - atom
page-toc:
    active: true
---

# Editores
La manera más fácil de trabajar y editar los archivos del **Howto** es a través de un editor de texto con Git integrado.

Como mencionamos antes, por razones prácticas, solo veremos cómo hacerlo con el **editor de texto Atom**, aunque pueden elegir utilizar cualquier otro. El funcionamiento es similar en casi todos los casos. Junto a cada paso a seguir en el editor también veremos los comandos de Git en la terminal para aprender y entender qué están haciendo Atom y Git.

Intentaremos agregar más guías sobre el uso de otros editores en el futuro. Si quieren y tienen tiempo, pueden también escribir uno ustedes acerca de sus editores prefereidos y podemos agregarlos a esta sección.


## [Atom](atom/interface)
