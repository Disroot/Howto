---
title: How-to Contribute
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---

# Contribute

![](contribute.jpg)

We think that knowledge is a collective construction: the result of working together and cooperatively, as a community. And whether the contributions take the form of a donation, writing/translating a tutorial or reporting bugs, they are all essentially personal time devoted to others. A bit like love.

The main goal of this part of the **Disroot** project is to provide clear and accessible information about how to use and configure the services we offer and, moreover, to do so in as many languages as possible.
It consists mostly of guides and tutorials that evolve as the software does. So we are always looking for help to check, write and translate howtos.

So, for those of you who may want to contribute by donating your time and knowhow, we have tried to channel all the efforts through this section.

Here you'll find basic information and guidelines for different ways to contribute, from feedback to write a how-to or translate them to your language.

---

# Table of Contents

## · [Howto Procedure & Tools](procedure)
## · [Git: Configuration & set-up](git)
## · [Working with a text editor](git/editors)
