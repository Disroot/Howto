---
title: Kiel kontribui
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---

# Kontribui

![](contribute.jpg)

Ni pensas ke la scio estas kolektiva konstruo: la rezulto de laboro are kaj kolektive kiel komunumo. Kaj jam esti ke la kontribuoj akiros la formo de dono, skribante/tradukante lerniloj aŭ raportante eraroj, ĉiuj estas esence persona tempo dediĉata al aliaj homoj. Iom kiel la amo.

La ĉefa objektivo de ĉi tiu parto de la projekto **Disroot** estas doni klara kaj atingebla informo pri kiel uzi kaj agordi la servoj kiu ni oferas kaj, krom, fari en la plej granda ebla kvanto da lingvoj. Ĝi konsistas pleje je gvidiloj kaj lerniloj kiu disvolvas konforme la programoj faras. Pro tio ĉiam ni serĉas helpon por revizii, skribi kaj traduki lernolibro.

Tiel ke, por kiu volas kontribui donante sia tempo kaj scioj, ni provis gvidi ĉiuj niaj penoj tra tiu ĉi sekcio.

Tie ĉi ili trovos baza informo kaj gvidiloj por la diversaj manieroj de kontribui, ekde komenti ĝis skribi lernolibro aŭ traduki ilin al siaj lingvoj.

---

# Enhavo

## · [Procedo kaj iloj por gvidiloj](procedure)
## · [Git: Agordoj kaj preparado](git)
## · [Laborante kun teksredaktilo](git/editors)

## · [Ĝeneralaj linioj pri la enhavo](styleguide)
