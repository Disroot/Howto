---
title: 'How-to Contribute: Style guide'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - style
page-toc:
    active: true
---

# Style Guide

![](h2_guide.jpg)

The purpose of this section is to provide some general guidelines to keep in mind when writing or translating a tutorial for the **Disroot's [How-to site](https://howto.disroot.org)** and thus maintain a certain coherence in the structure of the documentation, both visually and in terms of writing.


---
# Table of Content
- ### [Structure](structure)
Directories and pages

- ### [Content](content)
Writing, translating and visual content
