---
title: 'Information guidelines'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - style
page-toc:
    active: true
---

# Building the information guidelines

## First question: What can we write about?
This question is already partially answered in the main page of this site:
> *"We aim to cover all the services, with all its **Disroot**'s provided features, for all platforms and/or Operating Systems as well as the largest possible number of clients for the most widely used devices"*

So we can write about the software provided by **Disroot**, how they work and what can we do with them. That seems quite evident until we ask ourselves if we all use the same operating system in our computers or mobile devices.

The fact that our focus is exclusively on the use, promotion and spreading of **free and open source software** (and therefore our howtos are written mainly for GNU/Linux) should not make us forget that there are many people who choose to use proprietary software and their approach to our services is not related to ethical or political reasons but pragmatic.

That is why we explicitly say:

> *"for all platforms and/or Operating Systems as well as the largest possible number of clients for the most widely used devices"*

A basic list of the main operating systems we try to cover would be the following:

- **GNU/Linux**, **Microsoft Windows** and **Apple macOS** on desktop and laptop computers and
- **Google Android**, **Apple iOS**, **Jolla Sailfish OS** and **Microsoft Windows Mobile** on mobile devices.

!! **Everyone, no matter the operating system they use, can collaborate by writing how to use a service. Remember that the most important thing is to make the information available to anyone.**

## Second question: Who are the howtos aimed at?
In principle, any person over 16 years of age can use the **Disroot** services. But we can also think about the fact we all have, for example, relatives and acquaintances who we would like to introduce to the use of socially ethical and technically useful tools or maybe they just want to do it by themselves.

So when writing a howto we must think that the information must be understandable to anyone, regardless of age or technical knowledge. Of course, there are always software that are more complex than others, but that should not mean that we do not make the effort to try to make it possible for anyone to learn them.

This leads to the following question.

## Third question: What type of language should we use?
In order to make the howtos as accessible as possible, not only to the users but also to those who help us translate them, we must use English in the least informal way we can. This means avoiding the use of technical jargon, slang or linguistic contractions (for example, it is preferable to write "it will not" instead of "it won't"). We must also keep in mind that these guides could be used or translated by other projects that provide similar services or software and we want the task of adapting them to consume as little of their time as possible.

In those cases where the use of technical terms is unavoidable, we will need to explain briefly what they mean or refer to an external source (i.e., a Wikipedia article) that does so.

## Fourth question: Regarding translations, should they be literal?
Although there is not a specific and effective method, there are, at least, two important things we should keep in mind when doing a translation.
1. **Understand the text of the howto integrally**. Before we start translating, it is advisable to read the entire text and "capture" its essence: "what is it about", "how the information is presented and/or structured", "is it hard/easy to understand", etc. This will help us to identify the context of the words and expressions that appear in a given document and which meaning depends on it.
2. **Follow the original text is fundamental as is preserving its fluidity**. Sometimes we could find words or expressions that are really hard to translate into our language or even untranslatable. So we need to be careful not to omit them or change their original meaning too much because this can affect the overall meaning of a line, a paragraph or an entire document. If we have doubts about certain adaptations we can always consult or ask in the Howto chat room.

So the literalness of a translation does not always result in a coherent text. When it is necessary to make adaptations of expressions or words, these must be as faithful as possible to the original meaning of the document.

Finally, we will always need to proofread our translations before submitting them. It is very common to find small recurring mistakes or inconsistencies and correcting them in time will prevent a translation from being returned to us.
