---
title: 'Content guidelines'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - style
page-toc:
    active: true
---

# Content guidelines
There are several ways to write or translate a guide, no doubt. And if we search on the Internet we will find all kind of styles: from very technical to some very funny ones, passing through some quite boring, some visually impressive and others certainly spartan in more than one aspect. But they all have one thing in common: **the information**. That is our raw material.  

The **Howto Project** is about providing, not just any information, but useful information about the software available on the **Disroot** platform in the clearest and most accessible way we can. But how to achieve this?

Well, we can ask ourselves some questions and see if we can build together some definitions to guide us in the task.
