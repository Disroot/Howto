---
title: 'Jak přispět: Příručka stylu'
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - style
page-toc:
    active: true
---

# Příručka stylu

![](h2_guide.jpg)

Účelem této části je poskytnout několik obecných pokynů, které je třeba mít na paměti při psaní nebo překladu návodu pro **Disrootův [How-to site](https://howto.disroot.org)** a zachovat tak určitou soudržnost struktury dokumentace, a to jak po vizuální, tak po písemné stránce.


---
# Obsah
- ### [Struktura](structure)
Adresáře a stránky

- ### [Obsah](content)
Psaní, překlady a vizuální obsah
