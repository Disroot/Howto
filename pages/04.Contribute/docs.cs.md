---
název: Jak přispět
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---

# Přispět

![](contribute.jpg)

Domníváme se, že znalosti jsou kolektivní konstrukcí: výsledkem společné práce a spolupráce jako společenství. A ať už mají příspěvky podobu daru, psaní/překladu návodu nebo hlášení chyb, všechny jsou v podstatě osobním časem věnovaným ostatním. Trochu jako láska.

Hlavním cílem této části projektu **Disroot** je poskytovat jasné a přístupné informace o tom, jak používat a konfigurovat nabízené služby, a navíc tak činit v co největším počtu jazyků.
Skládá se převážně z návodů a tutoriálů, které se vyvíjejí spolu s vývojem softwaru. Proto stále hledáme pomoc při kontrole, psaní a překládání návodů.

Proto jsme se pro ty z vás, kteří by chtěli přispět věnováním svého času a know-how, pokusili nasměrovat veškeré úsilí prostřednictvím této sekce.

Zde najdete základní informace a pokyny k různým způsobům, jak přispět, od zpětné vazby až po napsání how-to nebo jejich překlad do vašeho jazyka.

---

# Obsah

## - [Howto Postup a nástroje](procedure)
## - [Git: konfigurace a nastavení](git)
## - [Práce s textovým editorem](git/editors)
