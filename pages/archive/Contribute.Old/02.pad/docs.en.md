---
title: How-to Contribute: Pads
published: false
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
page-toc:
    active: false
---


# Open Task

This is a good example for the work still to do. Until now, no one was found to write this Tutorial. Maybe you want to make it your first project?
