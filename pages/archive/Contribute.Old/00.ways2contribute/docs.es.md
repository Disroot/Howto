---
title: Cómo contribuir: Elige la manera
published: true
taxonomy:
    category:
        - docs
page-toc:
    active: false
---

|[GIT](/contribute/git)|[PAD](/contribute/pad)|[CORREO](/contribute/email)|[XMPP](/contribute/xmpp)|
|:--:|:--:|:--:|:--:|:--:|
|Puedes trabajar fuera de línea, escribiendo una guía o mejorando una, y luego "mandar" los cambios a través de Git.<br><br>**Nivel técnico requerido**: Muy básico.|Puedes trabajar en línea sobre un archivo de texto colaborativo, escribiendo o modificando un manual, luego comunicarte con el equipo de Manuales y Guías de Disroot.<br><br>**Nivel técnico requerido**: ninguno.|Puedes escribir o modificar una guía de la manera que lo desees, y enviarnos un correo con el trabajo terminado. Si tienes sugerencias o comentarios sobre los Manuales, puedes comunicarte con nosotros por este mismo medio.<br><br>**Nivel técnico requerido**: básico.|A través del foro puedes subir, escribir o compartir un manual o guía y también hacer sugerencias, opinar, etc. <br><br>**Nivel técnico requerido**: muy básico.|Puedes comunicarte con nosotros en nuestra sala XMPP de tutoriales de Disroot.<br><br>**Nivel técnico requerido**: muy básico.|
