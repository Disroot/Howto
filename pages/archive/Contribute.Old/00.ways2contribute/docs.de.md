---
title: "How-to: Mitwirken - Wähle Deinen Weg"
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - contribute
        - Mitwirken
page-toc:
    active: false
---

# Wie kann ich an den How-to mitwirken?

Es gibt verschiedene Wege, zwischen denen Du wählen kannst:

#### [GIT](/contribute/git)
Du kannst offline arbeiten, ein neues How-to schreiben oder ein bestehendes verbessern, und die Änderungen dann durch git zu **Disroot** "schieben"<br><br>**erforderliche Technikkenntnisse**: Anfänger.

#### [PAD](/contribute/pad)
Du kannst an einer gemeinschaftlichen Online-Textdatei arbeiten, ein How-to schreiben oder verändern, und dann mit dem **Disroot** How-to-Team kommunizieren.<br><br>**erforderliche Technikkenntnisse**: Keine.

#### [EMAIL](/contribute/email)
Du kannst ein How-to auf die von Dir gewünschte Art und Weise schreiben oder verändern und uns eine Email mit der fertigen Arbeit schicken. Wenn Du Vorschläge oder Feedback zu den How-to loswerden möchtest, kannst Du ebenfalls auf diesem Weg mit uns in Kontakt treten. <br><br>**erforderliche Technikkenntnisse**: Anfänger.

#### [XMPP](/contribute/xmpp)
Du kannst über unsere **Disroot** How-To Gruppe mit uns kommunizieren.<br><br>**erforderliche Technikkenntnisse**: Absoluter Anfänger.

----
### [Disroot Übersetzungen](/contribute/translations_procedure)
Wenn Du mitwirken möchtest, indem Du **Disroot How-to** in Deine Sprache übersetzt, sieh Dir bitte diese Anleitung an.

### [Disroots How-To Gestaltungsrichtlinie](/contribute/styleguide)
Einige grundlegende Richtlinien für den Inhalt und Formvorgaben für die How-to.
