---
title: Preguntas Frecuentes
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - correo
page-toc:
    active: true
---

# Migración a Roundcube: Preguntas Frecuentes

## ¿Dónde puedo saber más sobre Roundcube?
Tenemos una guía de **Roundcube** que puedes encontrar [aquí](../../roundcube).

## ¿Necesitaré utilizar credenciales de inicio de sesión diferentes para acceder a mi correo?
No, tus credenciales de **Disroot** serán las mismas de siempre.

## ¿Necesitaré reconfigurar mi cliente del móvil o del escritorio?
No, esta migración no debería afectar las configuraciones de tus clientes de correo.

## ¿Cómo accederé a Roundcube?
De la misma manera que lo haces habitualmente al correo web.

## ¿Qué pasará con mis correos y contactos?
Todos tus correos y contactos serán automáticamente transferidos a **Roundcube**. De todas maneras, te sugerimos fuertemente respaldar tus contactos. En esta misma guía podrás averiguar cómo hacerlo.

## ¿Puedo volver al correo web anterior?
No, no puedes.

## Mis contactos no migraron, ¿qué debo hacer?
Primero que todo, no entres en pánico. Si tus contactos no migraron, por favor, contáctanos y lo arreglaremos.

**Finalmente, si notas cualquier cosa inusual, tienes preguntas o sugerencias, por favor, contáctanos a soporte.**
