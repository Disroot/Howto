---
title: Guía de Migración de Roundcube
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - correo
page-toc:
    active: false
---

# Guía de Migración de Roundcube

![](thumb.png)

Desde el 16 de Julio de 2022, la solución de correo web de **Disroot** es **Roundcube** mientras que **SnappyMail** será discontinuado.

Hasta el 1ro de Julio de 2023, ambos clientes web estarán disponibles para dar tiempo a todes de hacer la transición, después de esa fecha solo Roundcube será accesible.

Aunque no deberían tener ningún inconveniente con dicha transición, les recomendamos encarecidamente que previamente hagan un respaldo de sus contactos. A continuación encontrarán una breve guía paso a paso que les ayudará a hacerlo sin problemas.

Comencemos...

----

# Índice
##  1. [Migración: Preguntas frecuentes](faq)
##  2. [Respaldar/exportar e importar sus contactos](backup)
