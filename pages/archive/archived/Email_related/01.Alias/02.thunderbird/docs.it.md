---
title: Email Alias: Setup on Thunderbird
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
page-toc:
    active: true
---

# Imposta alias su Thunderbird

Innanzitutto, avvia **Thunderbird** e vai alle impostazioni dell'account facendo clic con il pulsante destro del mouse sul tuo account. 

![](en/identity_settings.gif)

In basso a destra della finestra delle impostazioni, hai il pulsante delle impostazioni **"Gestisci identità..."**.

Una volta in Identity Manager, puoi aggiungere un nuovo alias premendo il pulsante **"Aggiungi..."** e compilando il modulo: 

![](en/identity_add.gif)

# Imposta default
Se desideri impostare un nuovo alias e-mail come predefinito, seleziona l'alias e-mail e fai clic sul pulsante **"Imposta predefinito"**. 

![](en/identity_default.gif)

# Invia una email
Per inviare e-mail con il tuo nuovo alias, fai clic sul campo **"Da"** e seleziona l'alias che desideri utilizzare dal menu a discesa, durante la composizione della posta. 

![](en/identity_send.gif)
