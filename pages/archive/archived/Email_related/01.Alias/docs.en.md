---
title: 'Alias setup'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
page-toc:
    active: false
---

# Email Aliases

Below you can find how to do set them up on various email clients.

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [FairEmail](fairemail)
- [Mail iOS](mailios)
