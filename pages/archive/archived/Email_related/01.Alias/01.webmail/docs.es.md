---
title: Alias de correo: Configurar en el Webmail
published: true
visible: false
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - webmail
        - correo
        - alias
        - configuraciones
page-toc:
    active: false
---

## Set alias on Roundcube

Check our howto [here](https://howto.disroot.org/es/tutorials/email/webmail/roundcube/settings/identities)