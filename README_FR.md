<p align="center">

![](h2.png)

</p>

<h1 align="center">Traductions et Guides de Disroot</h1>

<h2 align="center">· Participez et contribuez ·</h2>


<br>

Ce dépôt contient les fichiers du **Projet Howto de Disroot** et les informations pour créer, modifier et traduire ces fichiers.

L'objectif principal du **Projet Howto de Disroot** est la création, la maintenance et l'accessibilité des guides utilisateurs, tutoriels et configurations des différents services et logiciels fournis par **Disroot**. Et aussi fournir fournir les outils, les instructions et procédures pour toute personne souhaitant contribuer à la création, la modification et la traduction de ces guides.

<br>


# 1. Prérequis

  - a. Logiciels requis
  - b. Compte Forgejo
  - c. Demande d'accès à notre dépôt
  - d. Définition de votre nom d'utilisateur et email

Pour obtenir une copie du projet sur votre machine locale et commencer à travailler, vous devez suivre ces instructions.

## a. Logiciels requis

Tout d'abord, vous devez installer les logiciels suivants :

- ## git<br>

Un logiciel de gestion de versions, pour suivre les modifications apportées aux guides et aux traductions au fur et à mesure de leur rédaction.<br>
· [**Qu'est-ce que git ?**](https://fr.wikipedia.org/wiki/Git) <br> · [**Téléchargement et instructions d'installation**](https://git-scm.com/downloads)

- ## Un éditeur de texte avec prise en charge des fichiers au format markdown<br>

_Logiciels suggérés :_<br>
· **VSCodium**, une distribution binaire sous licence libre de l'éditeur VS Code de Microsoft, gérée par la communauté. [[**Téléchargement et instructions d'installation**](https://vscodium.com/)]<br>
· **Neovim**, un éditeur de texte hyperextensible basé sur Vim. [[**Téléchargement et instructions d'installation**](https://github.com/neovim/neovim/wiki/Installing-Neovim)]<br>
· **Kate Editor**, un éditeur de texte riche en fonctionnalités. [[**Téléchargement et instructions d'installation**](https://kate-editor.org/get-it/)]

<br>

## b. Compte Forgejo

Pour pouvoir envoyer les modifications que vous faites aux guides, qu'il s'agisse de traduction, de modification, ou de rédaction, vous devez avoir un compte sur une instance de **Forgejo**.

Pour créer un compte dans l'instance où se trouve notre dépôt, rendez-vous [**ici**](https://git.disroot.org/user/sign_up).

<br>

## c. Demande d'accès à notre dépôt

Afin de pouvoir livrer des modifications au dépôt git de **Disroot**, vous devez en demander l'accès. Cela se fait via notre page de projet git.

<br>

## d. Définition de votre nom d'utilisateur et email

Cela est nécessaire pour pouvoir envoyer votre travail de votre machine vers le dépôt distant. Pour définir les nom d'utilisateur et email sur git :

- ouvrez un terminal et lancez git<br>
`git init`<br>

- écrivez et complétez avec vos informations les commandes suivantes :<br>
`git config --global user.email "user@email"`<br>
`git config --global user.name "User Name"`

<br>


# 2. Création / traduction de guides

Le processus de création ou de traduction de guides n'est pas difficile et ne demande qu'un peu de pratique.

La procédure est plutôt simple :

1. Vous obtenez une copie des fichiers sur lesquels vous allez travailler ;
2. vous travaillez localement sur les fichiers dans votre ordinateur,
3. et une fois que vous avez terminé, vous les soumettez.


La première chose à faire est de cloner le dépôt, c'est-à-dire faire une copie locale des fichiers distants. Tout le travail sera effectué sur cette copie.

<br>

## a. Cloner le dépôt

Un dépôt git est un dossier de projet contenant les fichiers créés ou à traduire, ainsi que l'historique détaillé de ces modifications.

Pour cloner le dépôt, ouvrez un terminal dans le répertoire où vous souhaitez effectuer le clonage. Une fois dedans vous utilisez la commande `git clone`, une instruction pour cloner un dépôt en renseignant son adresse. Dans notre cas ce sera :

`git clone https://git.disroot.org/Disroot/howto`

Une fois le dépôt copié dans votre disque dur vous verrez un répertoire `howto` contenant tous les fichiers du projet. Il peut être déplacé ultérieurement à n'importe quel endroit de votre ordinateur.

<br>

## b. Instructions pour la création / traduction

Une fois le dépôt copié dans votre machine, vous pouvez commencer à créer, modifier et/ou traduire en suivant les étapes décrites dans ces guides :

- [Guide pour contribuer aux guides de **Disroot**](https://howto.disroot.org/fr/contribute)<br>

- [Guides et procédure de traduction](https://howto.disroot.org/fr/contribute/procedure)

<br>

# 3. Licence

La documentation **Disroot's Howto** est sous licence [Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
